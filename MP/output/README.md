# output_tables.Rdata

Performance statistics are defined in <https://github.com/flr/mse/blob/master/data-raw/statistics.R>

## mp_metrics

A data.table object containing the metrics used to monitor status and compute performance statistics along the intermediate year (2022) and projection years (2023-2040).

- mp: Short name of MP.
- year
- iter: Replicate.
- data: Value of metric.
- qname: name of metric (factor), one of:
  - Rec: yearly recruitment across both sexes.
  - SB: Spawning stock biomass, female only.
  - C: early catch.
  - F: Average fishing mortality across selected ages (0-30).

## om_metrics

Same metrics as above for the OM, years 1950-2022.

## perf

Performance statistics averaged across the tuning period (2034-2039)

- mp: Short name of MP.
- statistic: Short name of performance statistic.
- year: Final year of averaging period (2039)
- data: Value of performance statistic.
- iter: Replicate.
- name: Label name of performance statistic.

## year_perf

Performance statistics averaged over three time periods: short-term (2022-2025), medium-term (2026-2033), and long-term (2034-2039).

- mp: Short name of MP.
- statistic: Short name of performance statistic.
- year: Final year of averaging period.
- data: Value of performance statistic.
- iter: Replicate.
- name: Label name of performance statistic.

## kobe_perf

Yearly values of the Kobe-related performance statistics.

- mp: Short name of MP.
- statistic: Short name of performance statistic.
- year
- data: Value of performance statistic.
- iter: Replicate.
- name: Label name of performance statistic.

## nms

 Short and long names of MPs. Long names code the name of the status estimator (perf), HCR (hcst), probability of being in the Kobe green used for tuning (05) and parameter used for tuning (targ).
