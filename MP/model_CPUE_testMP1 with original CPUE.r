  # model_tune.R - DESC
# SWO/MP/model_tune_perfect.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


source("model.R")
#load_all('~/Active/MSE_PKG@flr/mse')

library(doFuture)
plan(multicore, workers=25)
options(future.globals.maxSize= 700000000)


# SET output folder
outpath <- "model/cpue"




#cpue.hcr <-mse::cpue.hcr





#### TUNNING MPs ##  +15/-10IAV : ONLY FOR P(GREEN) = 60%  #######################################
MP1<-readRDS(paste0(outpath,"/cpueFast_06_targ1510IAV.Rds"))


MP1_test <- mp(om, oem=oem, control=MP1@control, args=mseargs, parallel=TRUE)
# CONTROL ------------


### change the oem method so that the CPUE index for the last historical years does not get over written

# first source the function below
oem@method <-    sampling.oem.custom




## CPUE fast with 
# k parameteres values were determined by inspecting a grid (see presentations TCMP 2023 and others)
controlFast <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=2.1, k2=2.1, k3=1.2, k4=1.2, dlow = 0.90, dupp = 1.15, target=0.7125))))

MP1_test <- mp(om, oem=oem, control=controlFast, args=mseargs, parallel=TRUE)


saveRDS(MP1_test, file=file.path(outpath, "MP1_test.Rds"),
  compress="xz")



cpueFast_06_targ1510IAV_retuned <- tunebisect(om, oem=oem, control=controlFast, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.5, 0.9)), prob=0.6, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueFast_06_targ1510IAV_retuned, file=file.path(outpath, "cpueFast_06_targ1510IAV_retuned.Rds"),
  compress="xz")

as.data.frame(args(control(cpueFast_06_targ1510IAV_retuned)$hcr))

#> as.data.frame(args(control(cpueFast_06_targ1510IAV_retuned)$hcr))
#   k1  k2  k3  k4 dlow dupp target
#1 2.1 2.1 1.2 1.2  0.9 1.15 0.7625


#outpath <- file.path("model", c("jabba", "hr", "cpue"))
outpath <- file.path("model", c(  "cpue"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)

# select only JABBA and CPUE with TAC stab
#files <- files[c(9:12,15,16,13,14)]
files <- files[c(9,10,14)]

# SELECT runs
 tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))),
  readRDS)


nms <- setNames(names(tuned),  nm=c("3.MP1_corrected and retuned","1.MP1_TCMP", "2.MP1_corrected"))

names(tuned) <- names(nms)



sur <- biomass(oem@observations$idx[[1]])
cpue<- lapply(tuned,function(x) biomass(oem(x)@observations$idx[[1]]))

cpue[[1]]<-window(cpue[[1]],start=1994)
cpue[[1]][,ac(1994:2022)] <- sur[,ac(1994:2022)]


cpue[[3]]<-window(cpue[[3]],start=1994)
cpue[[3]][,ac(1994:2022)] <- sur[,ac(1994:2022)]

ggsave("report/output/test_cpues.png",
plot(cpue[[1]],cpue[[2]],cpue[[3]])  +theme_bw()  + scale_colour_manual(name="MP run",values=c("red","blue","green"),labels=names(tuned)) + ggtitle("historical and simulated CPUE index") + geom_vline(xintercept=2023)
) 

yearMeans(sur[,ac(2020:2022)])
tracking(tuned[[1]])["mean.ind",ac(2023)]
tracking(tuned[[2]])["mean.ind",ac(2023)]


dat <- data.table(as.data.frame(iter(sur[,ac(2018:2022)],1)))
slope <- dat[, .(data = coef(lm(log(data + 1e-22) ~ year))[2])]
slope
tracking(tuned[[1]])["slope.ind",ac(2023)]
tracking(tuned[[2]])["slope.ind",ac(2023)]



# RENAME MPs



# EXTRACT metrics

# extract the tuned MP parameters
MPpoints <- lapply(tuned, function(x) as.data.frame(args(control(x)$hcr)))


mp_metrics <- lapply(tuned, function(x) metrics(stock(x), metrics=mets))

om_metrics <- FLQuants(metrics(window(om, end=2023), metrics=mets))




# COMPUTE performance
perf <- performance(tuned, statistics=statistics, metrics=mets,
years=c(list(ST=2024:2028, tune=2034:2038, MT = 2024:2038 , LT=2024:2041,
  tac=seq(2024, 2038, by=3))))
  
year_perf <- performance(tuned, metrics=mets, statistics=statistics[c("green","PSBlim")],
  years=(seq(2024, 2044)))

kobe_perf <- performance(tuned, metrics=mets, statistics=kobestatistics,
  years=2024:2044)
                                                                                  

# 
aggregate(data~mp, data=subset(perf,year=="tune" & statistic == "green"),FUN=mean)
aggregate(data~mp, data=subset(perf,year=="tune" & statistic == "green"),FUN=median)  
  


# Correct perf so catch is tAC
perfsave<-perf
perf$name[perf$statistic=="C"]     <- "mean(TAC)"
perf$name[perf$statistic=="IACC"]     <- "IAC(TAC)"


# MP colors: red, green and blue, 3 hues

#cols <- c("brown3", "brown1" , "darkorange3", "darkorange","#3B9812", "#63c03a","#00FF33", "#009933" ) 
cols <- c("brown3",  "darkorange")  #,"#00FF33", "#009933" ) 



# plotBPs
#source("plotBPs 2.R")

perfBP <- perf[
  statistic == "SBMSY" & year == 'MT' |
  statistic == "PSBlim" & year == 'MT' |
  statistic == 'green' & year == 'MT' |
  statistic == 'IACC' & year == 'tac' |
  statistic == 'C' & year %in% c('MT', 'ST'), ]
  
  
perfBP[name=='mean(TAC)' & year == 'MT', year := '']
perfBP[name=='mean(TAC)' & year == 'ST', year := '(Short~term)']

  

# adjust to meet the requirements from the group
perfBP$data[perfBP$statistic == "IACC"] <- perfBP$data[perfBP$statistic == "IACC"]/100

library(ggh4x)
scales <- list(
  scale_y_continuous(),
  scale_y_continuous(),
  scale_y_continuous(),
  scale_y_continuous(labels = scales::percent),
  scale_y_continuous(limits = c(0, 40000)),
  scale_y_continuous(limits = c(0, 40000)))

 
ggsave("report/output/test2.png",
plotBPs(perfBP, statistics=c("SBMSY", "green", "PSBlim", "C", "IACC")) +
  ylim(c(0,NA)) +           
  scale_fill_discrete(type=cols) +
  theme(legend.position = "bottom") +
  facetted_pos_scales(y = scales) )
#



 
perfSum <- perf[
  statistic == "PSBMSY" & year == 'MT' |
  statistic == "PSBlim" & year == 'MT' |
  statistic == "risk1" & year == 'MT' |
  statistic == "risk2" & year == 'MT' |
  statistic == "risk3" & year == 'MT' |
  statistic == "SBMSY" & year == 'MT' |
  statistic == 'green' & year == 'tune' |
  statistic == 'CMSY' & year == 'MT' |
  statistic == 'maxTACup' & year == 'MT' |
  statistic == 'maxTACdown' & year == 'MT' |
  statistic == 'nTACchange' & year == 'MT' |
  statistic == 'IACC' & year == 'tac' |
  statistic == 'C' & year == 'MT' , ]



print(summTable(perfSum,statistics=c("C",'nTACchange',"IACC","PSBMSY","PSBlim")), type="html", file="report/output/summary_test.html")


    data<-perfSum
    stts<-c("C",'nTACchange',"IACC","PSBMSY","PSBlim") 
    probs<-c(0.1, 0.5, 0.9)
   # data <- data[statistic %in% stts, ]
    data[, `:=`(name, foo(name))]
    qdata <- data[, as.list(quantile(data, probs = probs, na.rm = TRUE)),
        keyby = list(statistic, name, mp)]
        attention la ligne suivant chamboule l ordre des mp!!!!!!!
    qdata$"50%"[qdata$statistic=="green"]     <-       round(aggregate(data~mp ,subset(data,statistic=="green") ,mean)$data,2)
    qdata[, `:=`(fig, paste0(format(round(`50%`, 2), digits = 3,
        scientific = FALSE, trim = TRUE), " (", format(round(`10%`,
        2), digits = 1, trim = TRUE, scientific = FALSE), "-",
        format(round(`90%`, 2), digits = 1, trim = TRUE, scientific = FALSE),
        ")"))]
    qtab <- dcast(qdata, mp ~ name, value.var = "fig")


fwrite(qtab,"report/output/custom_summary_test.txt", sep=";",col.names = T, row.names = FALSE)

 qtab<-as.data.frame(qtab)

write.csv(qtab,file="report/output/custom_summary_test.csv")


 
# FINAL table

print(
resTable(perf[year == "ST", ], statistics=statistics),
type="html", file="report/output/full_table_ST_test.html")

print(
resTable(perf[year == "MT", ], statistics=statistics),
type="html", file="report/output/full_table_MT_test.html")

print(
resTable(perf[year == "LT", ], statistics=statistics),
type="html", file="report/output/full_table_LT_test.html")

# EXTRA



















sampling.oem.custom <-   function (stk, deviances, observations, stability = 1, args, 
    tracking) 
{
    y0 <- ac(args$y0)
    dy <- ac(args$dy)
    dyrs <- ac(seq(args$dy - args$frq + 1, args$dy))
    if (any(!c("stk", "idx") %in% names(observations))) 
        stop("observations(oem) must have elements 'stk' and 'idx'.")
    simp <- (dim(catch.n(observations$stk))[c(3, 4, 5)] == 1) + 
        (dim(catch.n(stk))[c(3, 4, 5)] == 1) < 2
    if (any(simp)) 
        stk <- simplify(stk, c("unit", "season", "area")[simp], 
            harvest = FALSE)
    stk <- window(stk, start = y0, end = dy, extend = FALSE)
    if (!is.null(deviances$stk)) {
        for (i in names(deviances$stk)) {
            slot(stk, i)[, dyrs] <- do.call(i, list(object = stk))[, 
                dyrs] * deviances$stk[[i]][, dyrs] + 0.001
        }
        landings(stk)[, dyrs] <- computeLandings(stk[, dyrs])
        discards(stk)[, dyrs] <- computeDiscards(stk[, dyrs])
        catch(stk)[, dyrs] <- computeCatch(stk[, dyrs])
        observations$stk[, dyrs] <- stk[, dyrs]
    }
    idx <- observations$idx
    upi <- unlist(lapply(idx, function(x) unname(dims(x)$maxyear) > 
        args$dy))
    if (is.null(deviances$idx) | length(deviances$idx) == 0) {
        deviances$idx <- lapply(observations$idx, function(x) index.q(x) %=% 
            1)
    }
    
    
    if(dy!="2022")
    {
    idx[upi] <- Map(function(x, y, z) {
        dyrs <- intersect(dyrs, dimnames(y)$year)
        res <- survey(stk[, dyrs], x[, dyrs], sel = sel.pattern(x)[, 
            dyrs], index.q = index.q(x)[, dyrs] * y[, dyrs], 
            stability = z)
        if (sum(index(res)[, dyrs]) == 0) 
            index(res)[, dyrs] <- sqrt(.Machine$double.eps)
        index(res)[index(res) == 0] <- c(min(index(res)[index(res) > 
            0]/2))
        index(x)[, dyrs] <- index(res)
        return(window(x, end = dy))
    }, x = idx[upi], y = deviances$idx[upi], z = rep(stability, 
        length(idx))[upi])
     
        
    for (i in seq(idx[upi])) {
        yrs <- intersect(dyrs, dimnames(idx[upi][[i]])$year)
        observations$idx[upi][[i]][, dyrs] <- idx[upi][[i]][, 
            dyrs]
    }
    }
     
    list(stk = stk, idx = idx, observations = observations, tracking = tracking)
}
