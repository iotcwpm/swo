# model_tune_jabba.R - TUNE jabba.sa + hockeystick.hcr / trend.hcr MPs
# swo/MP/model_tune_jabba.R

# Copyright WMR, 2022
# Authors: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#          Thomas BRUNEL (WMR) <thomas.brunel@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


source("model.R")

library(doFuture)
plan(multicore, workers=25)
options(future.globals.maxSize= 700000000)


# PATH in anunna
outpath <- "model/jabba"


# --- TUNE(target) jabba.sa + hockeystick.hcr
control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=hockeystick.hcr,
    args=list(lim=0.10, trigger=0.40, target=mean(refpts(om)$MSY) * 0.90,
      metric="Bdepletion", output="catch", dlow=0.90, dupp=1.15))))

#
# P(kobe=green) = 0.6

jabba_hcst_06_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(26000,35000)), prob=0.6, tol=0.02, maxit=12,
  parallel=TRUE)

saveRDS(jabba_hcst_06_targ, file=file.path(outpath, "jabba_hcst_06_targ.Rds"),
  compress="xz")

rm(jabba_hcst_06_targ)

# P(kobe=green) = 0.7



jabba_hcst_07_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(22000,29000)), prob=0.7, tol=0.02, maxit=12,
  parallel=TRUE)

saveRDS(jabba_hcst_07_targ, file=file.path(outpath, "jabba_hcst_07_targ.Rds"),
  compress="xz")

rm(jabba_hcst_07_targ)





# --- TUNE(target) jabba.sa + catchstick.hcr#
control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=catchstick.hcr, args=list(target=mean(refpts(om)$MSY,sloperatio=0.25),
    dlow=0.90, dupp=1.15))))
 
 
#TEST
idx <- sample(seq(500), 2)
oms <- iter(om,  idx)
oems <- iter(oem, idx)
 
te <- mp(om, oem=oem, ctrl=control,  args=mseargs)



# P(kobe=green) = 0.6

jabba_catchstick_06_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(22000,45000)), prob=0.6, tol=0.02, maxit=12,
  parallel=TRUE)

saveRDS(jabba_catchstick_06_targ, file=file.path(outpath, "jabba_catchstick_06_targ.Rds"),
  compress="xz")

rm(jabba_catchstick_06_targ)


# P(kobe=green) = 0.7

jabba_catchstick_07_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(22000,41000)), prob=0.7, tol=0.02, maxit=12,
  parallel=TRUE)

saveRDS(jabba_catchstick_07_targ, file=file.path(outpath, "jabba_catchstick_07_targ.Rds"),
  compress="xz")

rm(jabba_catchstick_07_targ)



#############################################
# Robustness HOCKEYSTIK and CATCHSTICK
##############################################


#### robustness test to a 10% overshoot consistent
outpath2 <- "model/test"

iem <- FLiem(method=noise.iem,  args=list(noise=unitMeans(rec(om)) %=% (1.1)))


jabba_hcst_06_targ  <-  readRDS(file.path(outpath, "jabba_hcst_06_targ.Rds"))

jabba_hcst_06_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = jabba_hcst_06_targ@control, args = jabba_hcst_06_targ@args, parallel= TRUE)
saveRDS(jabba_hcst_06_targ_IE, file=file.path(outpath2, "jabba_hcst_06_targ_IE.Rds"),  compress="xz")
rm(jabba_hcst_06_targ_IE)

jabba_hcst_07_targ  <-  readRDS(file.path(outpath, "jabba_hcst_07_targ.Rds"))

jabba_hcst_07_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = jabba_hcst_07_targ@control, args = jabba_hcst_07_targ@args, parallel= TRUE)
saveRDS(jabba_hcst_07_targ_IE, file=file.path(outpath2, "jabba_hcst_07_targ_IE.Rds"),  compress="xz")
rm(jabba_hcst_07_targ_IE)


jabba_catchstick_06_targ  <-  readRDS(file.path(outpath, "jabba_catchstick_06_targ.Rds"))

jabba_catchstick_06_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = jabba_catchstick_06_targ@control, args = jabba_catchstick_06_targ@args, parallel= TRUE)
saveRDS(jabba_catchstick_06_targ_IE, file=file.path(outpath2, "jabba_catchstick_06_targ_IE.Rds"),  compress="xz")
 rm(jjabba_catchstick_06_targ_IE)

jabba_catchstick_07_targ  <-  readRDS(file.path(outpath, "jabba_catchstick_07_targ.Rds"))

jabba_catchstick_07_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = jabba_catchstick_07_targ@control, args = jabba_catchstick_07_targ@args, parallel= TRUE)
saveRDS(jabba_catchstick_07_targ_IE, file=file.path(outpath2, "jabba_catchstick_07_targ_IE.Rds"),  compress="xz")
rm(jabba_catchstick_07_targ_IE)



#### robustness test to a 15% overshoot first management cycle


iem <- FLiem(method=noise.iem,  args=list(noise=unitMeans(rec(om)) %=% (1)))
iem@args$noise[,ac(2024:2026)] <-1.15

jabba_hcst_06_targ  <-  readRDS(file.path(outpath, "jabba_hcst_06_targ.Rds"))

jabba_hcst_06_targ_IE2<- mp (om = om, oem = oem, iem = iem, ctrl = jabba_hcst_06_targ@control, args = jabba_hcst_06_targ@args, parallel= TRUE)
saveRDS(jabba_hcst_06_targ_IE2, file=file.path(outpath2, "jabba_hcst_06_targ_IE2.Rds"),  compress="xz")
 rm(jabba_hcst_06_targ_IE2)

jabba_hcst_07_targ  <-  readRDS(file.path(outpath, "jabba_hcst_07_targ.Rds"))

jabba_hcst_07_targ_IE2<- mp (om = om, oem = oem, iem = iem, ctrl = jabba_hcst_07_targ@control, args = jabba_hcst_07_targ@args, parallel= TRUE)
saveRDS(jabba_hcst_07_targ_IE2, file=file.path(outpath2, "jabba_hcst_07_targ_IE2.Rds"),  compress="xz")
rm(jabba_hcst_07_targ_IE2)

jabba_catchstick_06_targ  <-  readRDS(file.path(outpath, "jabba_catchstick_06_targ.Rds"))

jabba_catchstick_06_targ_IE2<- mp (om = om, oem = oem, iem = iem, ctrl = jabba_catchstick_06_targ@control, args = jabba_catchstick_06_targ@args, parallel= TRUE)
saveRDS(jabba_catchstick_06_targ_IE2, file=file.path(outpath2, "jabba_catchstick_06_targ_IE2.Rds"),  compress="xz")
rm(jabba_catchstick_06_targ_IE2)

jabba_catchstick_07_targ  <-  readRDS(file.path(outpath, "jabba_catchstick_07_targ.Rds"))

jabba_catchstick_07_targ_IE2<- mp (om = om, oem = oem, iem = iem, ctrl = jabba_catchstick_07_targ@control, args = jabba_catchstick_07_targ@args, parallel= TRUE)
saveRDS(jabba_catchstick_07_targ_IE2, file=file.path(outpath2, "jabba_catchstick_07_targ_IE2.Rds"),  compress="xz")
rm(jabba_catchstick_07_targ_IE2)



#### robustness test to a 2 year management lag

outpath2 <- "model/test"

# first need to feel in some catch in the OM for 2024...
ctrlIY<- fwdControl(year=2024, relYear=2023, quant="fbar", value=1, minAge=2, maxAge=8)

om2 <- fwd(om,control=ctrlIY)


# SUBSAMPLE for testing
#idx <- sample(seq(500), 2)
#oms <- iter(om2,  idx)
#oems <- iter(oem, idx)
#

jabba_hcst_06_targ  <-  readRDS(file.path(outpath, "jabba_hcst_06_targ.Rds"))
#jabba_hcst_06_targ@control$hcr@method  <- hockeystick.hcr
mpargs2<-jabba_hcst_06_targ@args
mpargs2$management_lag  <- 2

jabba_hcst_06_targ_lag<- mp (om = om2, oem = oem, , ctrl = jabba_hcst_06_targ@control, args = mpargs2, parallel= TRUE)
saveRDS(jabba_hcst_06_targ_lag, file=file.path(outpath2, "jabba_hcst_06_targ_lag.Rds"),  compress="xz")


jabba_hcst_07_targ  <-  readRDS(file.path(outpath, "jabba_hcst_07_targ.Rds"))
#jabba_hcst_07_targ@control$hcr@method  <- hockeystick.hcr
mpargs2<-jabba_hcst_07_targ@args
mpargs2$management_lag  <- 2

jabba_hcst_07_targ_lag<- mp (om = om2, oem = oem, , ctrl = jabba_hcst_07_targ@control, args = mpargs2, parallel= TRUE)
saveRDS(jabba_hcst_07_targ_lag, file=file.path(outpath2, "jabba_hcst_07_targ_lag.Rds"),  compress="xz")



jabba_catchstick_06_targ  <-  readRDS(file.path(outpath, "jabba_catchstick_06_targ.Rds"))
#jabba_catchstick_06_targ@control$hcr@method  <- hockeystick.hcr
mpargs2<-jabba_catchstick_06_targ@args
mpargs2$management_lag  <- 2

jabba_catchstick_06_targ_lag<- mp (om = om2, oem = oem, , ctrl = jabba_catchstick_06_targ@control, args = mpargs2, parallel= TRUE)
saveRDS(jabba_catchstick_06_targ_lag, file=file.path(outpath2, "jabba_catchstick_06_targ_lag.Rds"),  compress="xz")


jabba_catchstick_07_targ  <-  readRDS(file.path(outpath, "jabba_catchstick_07_targ.Rds"))
#jabba_catchstick_07_targ@control$hcr@method  <- hockeystick.hcr
mpargs2<-jabba_catchstick_07_targ@args
mpargs2$management_lag  <- 2

jabba_catchstick_07_targ_lag<- mp (om = om2, oem = oem, , ctrl = jabba_catchstick_07_targ@control, args = mpargs2, parallel= TRUE)
saveRDS(jabba_catchstick_07_targ_lag, file=file.path(outpath2, "jabba_catchstick_07_targ_lag.Rds"),  compress="xz")






#### robustness test to a recruitment failre

outpath2 <- "model/test"

om2<-om
residuals(om2@sr)[,ac(2024:2026)]  <-   0.1     # recruitment is 30% of the SR model prediction)

# SUBSAMPLE for testing
#idx <- sample(seq(500), 2)
#oms <- iter(om2,  idx)
#oems <- iter(oem, idx)
#

jabba_hcst_06_targ  <-  readRDS(file.path(outpath, "jabba_hcst_06_targ.Rds"))
jabba_hcst_06_targ_Rfail<- mp (om = om2, oem = oem, , ctrl = jabba_hcst_06_targ@control, args = jabba_hcst_06_targ@args, parallel= TRUE)
saveRDS(jabba_hcst_06_targ_Rfail, file=file.path(outpath2, "jabba_hcst_06_targ_Rfail.Rds"),  compress="xz")


jabba_hcst_07_targ  <-  readRDS(file.path(outpath, "jabba_hcst_07_targ.Rds"))
jabba_hcst_07_targ_Rfail<- mp (om = om2, oem = oem, , ctrl = jabba_hcst_07_targ@control, args = jabba_hcst_07_targ@args, parallel= TRUE)
saveRDS(jabba_hcst_07_targ_Rfail, file=file.path(outpath2, "jabba_hcst_07_targ_Rfail.Rds"),  compress="xz")



jabba_catchstick_06_targ  <-  readRDS(file.path(outpath, "jabba_catchstick_06_targ.Rds"))
jabba_catchstick_06_targ_Rfail<- mp (om = om2, oem = oem, , ctrl = jabba_catchstick_06_targ@control, args = jabba_catchstick_06_targ@args, parallel= TRUE)
saveRDS(jabba_catchstick_06_targ_Rfail, file=file.path(outpath2, "jabba_catchstick_06_targ_Rfail.Rds"),  compress="xz")


jabba_catchstick_07_targ  <-  readRDS(file.path(outpath, "jabba_catchstick_07_targ.Rds"))
jabba_catchstick_07_targ_Rfail<- mp (om = om2, oem = oem, , ctrl = jabba_catchstick_07_targ@control, args = jabba_catchstick_07_targ@args, parallel= TRUE)
saveRDS(jabba_catchstick_07_targ_Rfail, file=file.path(outpath2, "jabba_catchstick_07_targ_Rfail.Rds"),  compress="xz")

