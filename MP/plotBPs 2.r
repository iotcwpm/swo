# plotBPs.R - DESC
# /home/mosqu003/plotBPs.R

# Copyright (c) WUR, 2024.
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2

plotBPs <- function(data, statistics=unique(data$statistic), size=3,
  target=missing, limit=missing, reference=missing, changes=NULL,
  yminmax=c(0.10, 0.90), lowupp=c(0.25, 0.75), show.mean=NULL) {

  # CHECK quantiles
  if(any(c(length(yminmax), length(lowupp)) != 2))
    stop("'yminmax' and 'lowupp' must be both of length 2")

  # SUBSET statistics
  data <- data[statistic %in% statistics,]
  
  # ORDER name as of statistics
  data <- data[, name:=factor(name, levels=unique(name)[match(statistics,
    unique(statistic))], ordered=TRUE)]

  # ORDER mp as in input
  data[, mp:=factor(mp, levels=unique(mp))]

  dat <- data[, .(
    ymin=quantile(data, yminmax[1], na.rm=TRUE),
    lower=quantile(data, lowupp[1], na.rm=TRUE),
    middle=median(data, na.rm=TRUE),
    upper=quantile(data, lowupp[2], na.rm=TRUE),
    ymax=quantile(data, yminmax[2], na.rm=TRUE)),
  by=.(mp, statistic, name, year)]

  # MERGE name + year if needed
  id <- dat[, length(unique(year)) == 2, by=name]
  dat[name %in% as.character(id[id$V1, name]),
    name := as.character(paste0(name, " (", year, ")"))]

  # TODO: APPLY changes
  if(!is.null(changes)) {
    for (ch in names(changes)) {
      pat <- ifelse(identical(changes[[ch]], ""), paste0(" \\(", ch, "\\)"), ch)
      dat[grepl(ch, name), name:=sub(pat, changes[[ch]], name)]
    }
  }

  # MEAN for show.mean statistics
  if(!is.null(show.mean)) {
    midt <- data[statistic %in% show.mean, .(middle=mean(data, na.rm=TRUE)),
      by=.(mp, statistic, name)]$middle
    dat[statistic %in% show.mean, middle:=midt]
  }

  # PLOT
  p <- ggplot(dat,
    aes(x=mp, ymin=ymin, lower=lower, middle=middle,
      upper=upper, ymax=ymax, fill=mp)) +
    # data ~ mp, colour by mp
    # PLOT point by mp, useful if boxplot is very thin
    geom_point(data=dat[, .(middle=mean(middle)), by=.(mp, name)],
      aes(x=mp, y=middle), colour="black", size=size + size * 0.20,
      inherit.aes=FALSE) +
    geom_point(data=dat[, .(middle=mean(middle)), by=.(mp, name)],
      aes(x=mp, y=middle, fill=mp), shape=21, size=size, inherit.aes=FALSE) +
    # PLOT boxplot by mp
    geom_boxplot(stat="identity") +
    # PANELS per statistics
    facet_wrap(~name, scales='free_y', labeller="label_parsed") +
    xlab("") + ylab("") +
    # DELETE x-axis labels, LEGEND in 6th panel
    # TODO legend pos by no. of panels
    theme(axis.text.x=element_blank(), legend.position=c("right"),
    # DELETE legend title
    legend.title=element_blank())

  # TARGET
  if(!missing(target)) {
    dat <- data[statistic %in% names(target),]
    dat[, target:=unlist(target)[match(statistic, names(target))]]
    p <- p + geom_hline(data=dat, aes(yintercept=target), colour="green",
      linetype="longdash", size=1)
  }
  
  # LIMIT
  if(!missing(limit)) {
    dat <- data[statistic %in% names(limit),]
    dat[, limit:=unlist(limit)[match(statistic, names(limit))]]
    p <- p + geom_hline(data=dat, aes(yintercept=limit), colour="red",
      linetype="longdash", size=1)
  }

  # REFERENCE
  if(!missing(reference)) {
    dat <- data[statistic %in% names(reference),]
    dat[, reference:=unlist(reference)[match(statistic, names(reference))]]
    p <- p + geom_hline(data=dat, aes(yintercept=reference), colour="gray",
      linetype="longdash", size=1)
  }

  return(p)
}

