# report.R - DESC
# /report.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)
library(mseviz)
library(metR)

source("model.R")



# --- OUTPUT for the main plots 

load("output/outputTCMPMai2024_Rfail.Rdata")



# Correct perf so catch is tAC
perfsave<-perf
perf$name[perf$statistic=="C"]     <- "mean(TAC)"
perf$name[perf$statistic=="IACC"]     <- "IAC(TAC)"

# MP colors: red, green and blue, 3 hues


#cols <- c("brown3", "brown1" , "darkorange3", "darkorange","#3B9812", "#63c03a","#00FF33", "#009933" ) 
cols <- c("brown3", "brown1" , "darkorange3", "darkorange","#3B9812", "#63c03a")  #,"#00FF33", "#009933" ) 



pl <- subset(year_perf ,statistic %in% c("green","SBMSY"))
pl$data[pl$statistic == "SBMSY"] <- 5 * pl$data[pl$statistic == "SBMSY"]
pl$statistic[pl$statistic == "SBMSY"]  <- "SBlim"


Q10<- aggregate(data~year + mp ,   subset(pl,statistic == "SBlim"),function(x) quantile(x,0.10))
Q90<- aggregate(data~year + mp ,   subset(pl,statistic == "SBlim"),function(x) quantile(x,0.90))
Q50<- aggregate(data~year + mp ,   subset(pl,statistic == "SBlim"),function(x) quantile(x,0.50))

names(Q10)[3] <- "p10"
names(Q50)[3] <- "p50"
names(Q90)[3] <- "p90"

Q<- merge(Q10,merge(Q50,Q90))

mpprob<-data.frame(mp=paste0("MP",1:6),pGreen = rep(c("pGreen=60%","pGreen=70%"),3))

Q<-merge(Q,  mpprob,all.x=T)

ggsave("report/output/RiskPlot_Rfail_TCMP_Mai2024_Rfail.png",
ggplot(Q ,aes(year  , p50,group=mp)) + 
geom_ribbon(aes(ymin=p10,ymax=p90,group=mp,colour=mp,fill=mp), alpha=0.1)+
geom_line(aes(colour=mp))  + geom_hline(yintercept = 1) +ylab("SB/SBlim")   + 
scale_fill_manual(values=cols, name="mp" )  +
scale_colour_manual(values=cols, name="mp" ) + facet_grid(pGreen~.) + 
theme(axis.text.x=element_text(angle=90,hjust=1))  
)         








# plotBPs
perf<-perfsave
perfBP <- perf[
  statistic == "SBMSY" & year == 'MT' |
  statistic == "PSBlim" & year == 'MT' |
  statistic == 'green' & year == 'MT' |
  statistic == 'IACC' & year == 'tac' |
  statistic == 'C' & year %in% c('MT', 'ST'), ]


 
perfBP[name=='mean(TAC)' & year == 'MT', year := '']
perfBP[name=='mean(TAC)' & year == 'ST', year := '(Short~term)']

  
# adjust to meet the requirements from the group
perfBP$data[perfBP$statistic == "IACC"] <- perfBP$data[perfBP$statistic == "IACC"]/100

library(ggh4x)
scales <- list(
  scale_y_continuous(),
  scale_y_continuous(),
  scale_y_continuous(),
  scale_y_continuous(labels = scales::percent),
  scale_y_continuous(limits = c(0, 40000)),
  scale_y_continuous(limits = c(0, 40000)))

 
  
ggsave("report/output/bpsTCMP_Mai2024_Rfail.png",
plotBPs(perfBP, statistics=c("SBMSY", "green", "PSBlim", "C", "IACC")) +
  ylim(c(0,NA)) +           
  scale_fill_discrete(type=cols) +
  theme(legend.position = "right") +
  facetted_pos_scales(y = scales) )
#
#



perfTO <- perf[
  statistic == "SBMSY" & year == 'MT' |
  statistic == "PSBlim" & year == 'MT' |
  statistic == 'green' & year == 'MT' |
  statistic == 'IACC' & year == 'tac' |
  statistic == 'C' & year %in% c('MT'), ]


ggsave("report/output/tosTCMP_Mai2024_Rfail.png",
plotTOs(perfTO, x="C", y=c("SBMSY", "green", "PSBlim", "IACC")) +
  ylim(c(0,NA)) +
  scale_fill_discrete(type=cols) +
  guides(fill=guide_legend(ncol=1, byrow=FALSE))
)

# kobeMPs

ggsave("report/output/kobempsTCMP_Mai2024_Rfail.png",
kobeMPs(subset(perf,year=="MT"), x="SBMSY", y="FMSY", SBlim=0.40, Flim=1.4, Ftarget=NULL,
  SBtarget=NULL, probs=c(0.10, 0.50, 0.90), size=0.75, alpha=1) +
  scale_fill_discrete(type=cols) +
  guides(fill=guide_legend(ncol=1, byrow=FALSE))
)

# kobeTS

ggsave("report/output/kobetsTCMP_Mai2024_Rfail.png",
kobeTS(kobe_perf) + theme(legend.position="bottom")
)

# PLOT time series

its <- seq(500)[c(om_metrics$SB[,'2023']) %in%
  sort(c(om_metrics$SB[,'2023']))[c(50, 250, 450)]]

#its <- sample(1:500,3)
mp_metrics <- lapply(mp_metrics,function(x) window(x,start = 2024))
# F

ggsave("report/output/metricsFTCMP_Mai2024_Rfail.png",
plotOMruns(om_metrics$F, FLQuants(lapply(mp_metrics, "[[", "F")), iter=its , ylim = c(0,1.5))
)

# SSB

ggsave("report/output/metricsSBTCMP_Mai2024_Rfail.png",
plotOMruns(om_metrics$SB, FLQuants(lapply(mp_metrics, "[[", "SB")), iter=its)
)

# CATCH

ggsave("report/output/metricsCTCMP_Mai2024_Rfail.png",
plotOMruns(om_metrics$C, FLQuants(lapply(mp_metrics, "[[", "C")), iter=its)
)


# SUMMARY table


perfSum <- perf[
  statistic == "PSBMSY" & year == 'MT' |
  statistic == "PSBlim" & year == 'MT' |
  statistic == "risk1" & year == 'MT' |
  statistic == "risk2" & year == 'MT' |
  statistic == "risk3" & year == 'MT' |
  statistic == "SBMSY" & year == 'MT' |
  statistic == 'green' & year == 'tune' |
  statistic == 'CMSY' & year == 'MT' |
  statistic == 'maxTACup' & year == 'MT' |
  statistic == 'maxTACdown' & year == 'MT' |
  statistic == 'nTACchange' & year == 'MT' |
  statistic == 'IACC' & year == 'tac' |
  statistic == 'C' & year == 'MT' , ]

perf[statistic == "risk3" & year == 'MT']


print(summTable(perfSum,statistics=c("C",'nTACchange',"IACC","PSBMSY","PSBlim")), type="html", file="report/output/summary_tableTCMP_Mai2024_Rfail.html")
print(summTable(perfSum,statistics=c("C",'nTACchange',"IACC","PSBMSY","PSBlim")), type="latex", file="report/output/summary_tableTCMP_Mai2024_Rfail.png")

   data<-perfSum
    stts<-c("C",'nTACchange',"IACC","PSBMSY","PSBlim","risk1","risk2","risk3") 
    probs<-c(0.1, 0.5, 0.9)
   # data <- data[statistic %in% stts, ]
    data[, `:=`(name, foo(name))]
    qdata <- data[, as.list(quantile(data, probs = probs, na.rm = TRUE)),
        keyby = list(statistic, name, mp)]
    qdata$"50%"[qdata$statistic=="green"]     <-       round(aggregate(data~mp ,subset(data,statistic=="green") ,mean)$data,2)
    qdata[, `:=`(fig, paste0(format(round(`50%`, 2), digits = 3,
        scientific = FALSE, trim = TRUE), " (", format(round(`10%`,
        2), digits = 1, trim = TRUE, scientific = FALSE), "-",
        format(round(`90%`, 2), digits = 1, trim = TRUE, scientific = FALSE),
        ")"))]
    qtab <- dcast(qdata, mp ~ name, value.var = "fig")


fwrite(qtab,"report/output/custom_summary_tableTCMP_Mai2024_Rfail.txt", sep=";",col.names = T, row.names = FALSE)


# FINAL table

print(
resTable(perf[year == "ST", ], statistics=statistics),
type="html", file="report/output/full_table_ST_TCMP_Mai2024_Rfail.html")

print(
resTable(perf[year == "MT", ], statistics=statistics),
type="html", file="report/output/full_table_MT_TCMP_Mai2024_Rfail.html")

print(
resTable(perf[year == "LT", ], statistics=statistics),
type="html", file="report/output/full_table_LT_TCMP_Mai2024_Rfail.html")

# EXTRA

# DIFF mean ~ median Kobe(green)
# hist(perf[mp=='MP3' & statistic=='green',]$data)

# }}}


















