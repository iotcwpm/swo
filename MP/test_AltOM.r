# test.R - DESC
# /home/mosquia/Backlog/SWO_MSE@iotc/swo/MP/test.R

# Copyright (c) WUR, 2022.
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)
#source("utilities old.R")
source("utilities.R")


# DATA
load("data/om.Rdata")

# SET Ftarget and SBlim

refpts(om)$Ftarget <- refpts(om)$FMSY 
refpts(om)$SBlim <- refpts(om)$SBMSY * 0.20

# MSE arguments

mseargs <- list(iy=2022, data_lag=1, frq=3)

# TPERIOD, last OM year + 11:15
tperiod <- list(2034:2038)

# load statistics
data(statistics)


library(doParallel)
registerDoParallel(3)


### test a project at F=  Fmsy
ctrlFmsy<- fwdControl(list(year=2024:2046,  # projection for fishery
                              quant="fbar",
                              value= refpts(om)$FMSY))

projFmsy <- fwd(om,control=ctrlFmsy)




# same with alt OM
# DATA
load("data/om_Robstness_SA2023.Rdata")

# SET Ftarget and SBlim

refpts(om)$Ftarget <- refpts(om)$FMSY 
refpts(om)$SBlim <- refpts(om)$SBMSY * 0.20


projFmsy_Rob <- fwd(om,control=ctrlFmsy)


plot( FLStocks(OM=stock(projFmsy),  altOM= stock(projFmsy_Rob))) + xlim(1990,2046) + geom_vline(xintercept = 2024)



## MAKE a plot that combines the OM and the assessmentq()

x<-projFmsy
y<-projFmsy_Rob



 dat <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(x))  / refpts(x)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(x)) / refpts(x)$FMSY)
  
 dat2 <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(y))  / refpts(y)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(y)) / refpts(y)$FMSY) 
  
dat<-as.data.frame(dat,drop=T) 
dat2<-as.data.frame(dat2,drop=T)   

dat$basis <- "OM"
dat2$basis <- "altOM"

dat <- rbind(dat,dat2)
dat <- subset(dat, year < 2036 & year > 1990)

d1<-aggregate(data~ year + qname + basis , dat,median)
names(d1)[4] <- "median"
d2<-aggregate(data~ year + qname + basis , dat,function(x) quantile(x,0.05))
names(d2)[4] <- "Q05"
d3<-aggregate(data~ year + qname + basis , dat,function(x) quantile(x,0.90))
names(d3)[4] <- "Q95"

dd<-merge(d1,merge(d2,d3,all=T),all=T)


  p1 <- ggplot(subset(dd,qname=="SB/SB[MSY]"),aes(year,median,col=basis)) + geom_ribbon(aes(ymin = Q05,ymax=Q95,fill=basis),alpha=0.3) + 
    geom_line() + geom_hline(yintercept=1, linetype=2) + ylim(c(0,9))  + ylab("SB/SB[MSY]")   + geom_vline(xintercept = 2024)


  p2 <- ggplot(subset(dd,qname=="F/F[MSY]"),aes(year,median,col=basis)) + geom_ribbon(aes(ymin = Q05,ymax=Q95,fill=basis),alpha=0.3) + 
    geom_line() + geom_hline(yintercept=1, linetype=2)  + ylim(c(0,3))  + ylab("F/F[MSY]")  + geom_vline(xintercept = 2024)


  fin <- data.table(dat)[year == 2035, ]
  fin[,y_min := 0]
  fin[,y_max:= 6, by = qname]


  p3 <- ggplot(subset(fin,qname=="SB/SB[MSY]"), aes(y=data,fill = basis)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 6))+
    theme(legend.position="none") +
    xlim(c(0,1))   +    ylim(c(0,9)) +
    xlab("") + ylab("SB(2035)/SB[MSY]")+
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)
      
      
     
  p4 <- ggplot(subset(fin,qname=="F/F[MSY]"), aes(y=data,fill = basis)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 25))+
    theme(legend.position="none") +
    xlim(c(0,1.5))   +    ylim(c(0,3)) +
    xlab("") +  ylab("F(2035)/F[MSY]")+
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)
      

  # ASSEMBLE plot with c(4,1) horizontal proportions
 
  library(patchwork)
  p1 + p3 + p2 + p4 + plot_layout(widths = c(4, 4))     


