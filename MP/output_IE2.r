# output.R - PREPARE mode results: metrics, performance
# swo/MP/output.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2



library(mse)
library(mseviz)


# LOAD statistics & OM

source("model.R")


# TPERIOD, last OM year + 11:15

tperiod <- list(2034:2038)

# MPs list

#outpath <- file.path("model", c("jabba", "hr", "cpue"))
outpath <- file.path("model", c("test"))




files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)

# select only JABBA and CPUE with TAC stab
files <- files[grep("_IE2.Rds",files)]
#files <- files[c(1:4,7,8,5,6)]
files <- files[c(1:4,7,8)]

# SELECT runs

tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))),
  readRDS)

# RENAME MPs

nms <- setNames(names(tuned),  nm=paste0("MP", c(1:6)))

names(tuned) <- names(nms)

# EXTRACT metrics

mp_metrics <- lapply(tuned, function(x) metrics(stock(x), metrics=mets))

om_metrics <- FLQuants(metrics(window(om, end=2023), metrics=mets))

# COMPUTE performance

for (i in 1:6) catch(tuned[[i]]@om@stock)[,ac(2024:2026)] <- catch(tuned[[i]])[,ac(2024:2026)]/1.15

perf <- performance(tuned, statistics=statistics, metrics=mets,
years=c(list(ST=2024:2028, tune=2034:2038, MT = 2024:2038 , LT=2024:2041,
  tac=seq(2024, 2038, by=3))))
  
  
  
year_perf <- performance(tuned, metrics=mets, statistics=statistics[c("green","PSBlim")],
  years=(seq(2024, 2044)))

kobe_perf <- performance(tuned, metrics=mets, statistics=kobestatistics,
  years=2024:2044)
                                                                                    

# SAVE
save(mp_metrics, om_metrics, perf, year_perf, kobe_perf, 
  file="output/outputTCMPMai2024_IE2.Rdata", compress="xz")




