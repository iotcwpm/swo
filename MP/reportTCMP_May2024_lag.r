# report.R - DESC
# /report.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)
library(mseviz)
library(metR)

source("model.R")



# --- OUTPUT for the main plots 

load("output/outputTCMPMai2024_lag.Rdata")




# Correct perf so catch is tAC
perfsave<-perf
perf$name[perf$statistic=="C"]     <- "mean(TAC)"
perf$name[perf$statistic=="IACC"]     <- "IAC(TAC)"
perf$data[perf$statistic=="nTACchange"] <- perf$data[perf$statistic=="nTACchange"]-1


# MP colors: red, green and blue, 3 hues

#cols <- c("brown3", "brown1" , "darkorange3", "darkorange","#3B9812", "#63c03a","#00FF33", "#009933" ) 
cols <- c("brown3", "brown1" , "darkorange3", "darkorange","#3B9812", "#63c03a")  #,"#00FF33", "#009933" ) 


# plotBPs

perfBP <- perf[
  statistic == "SBMSY" & year == 'MT' |
  statistic == "PSBlim" & year == 'MT' |
  statistic == 'green' & year == 'MT' |
  statistic == 'IACC' & year == 'tac' |
  statistic == 'C' & year %in% c('MT', 'ST'), ]

 
  
 
perfBP[name=='mean(TAC)' & year == 'MT', year := '']
perfBP[name=='mean(TAC)' & year == 'ST', year := '(Short~term)']

  
# adjust to meet the requirements from the group
perfBP$data[perfBP$statistic == "IACC"] <- perfBP$data[perfBP$statistic == "IACC"]/100

library(ggh4x)
scales <- list(
  scale_y_continuous(),
  scale_y_continuous(),
  scale_y_continuous(),
  scale_y_continuous(labels = scales::percent),
  scale_y_continuous(limits = c(0, 40000)),
  scale_y_continuous(limits = c(0, 40000)))

 

  
ggsave("report/output/bpsTCMP_Mai2024_lag.png",
plotBPs(perfBP, statistics=c("SBMSY", "green", "PSBlim", "C", "IACC")) +
  ylim(c(0,NA)) +           
  scale_fill_discrete(type=cols) +
  theme(legend.position = "bottom") +
  facetted_pos_scales(y = scales) )
#
#



perfTO <- perfsave[
  statistic == "SBMSY" & year == 'MT' |
  statistic == "PSBlim" & year == 'MT' |
  statistic == 'green' & year == 'MT' |
  statistic == 'IACC' & year == 'tac' |
  statistic == 'C' & year %in% c('MT'), ]


ggsave("report/output/tosTCMP_Mai2024_lag.png",
plotTOs(perfTO, x="C", y=c("SBMSY", "green", "PSBlim", "IACC")) +
  ylim(c(0,NA)) +
  scale_fill_discrete(type=cols) +
  guides(fill=guide_legend(ncol=1, byrow=FALSE))
)

# kobeMPs

ggsave("report/output/kobempsTCMP_Mai2024_lag.png",
kobeMPs(subset(perf,year=="MT"), x="SBMSY", y="FMSY", SBlim=0.40, Flim=1.4, Ftarget=NULL,
  SBtarget=NULL, probs=c(0.10, 0.50, 0.90), size=0.75, alpha=1) +
  scale_fill_discrete(type=cols) +
  guides(fill=guide_legend(ncol=1, byrow=FALSE))
)

# kobeTS

ggsave("report/output/kobetsTCMP_Mai2024_lag.png",
kobeTS(kobe_perf) + theme(legend.position="bottom")
)

# PLOT time series

its <- seq(500)[c(om_metrics$SB[,'2023']) %in%
  sort(c(om_metrics$SB[,'2023']))[c(50, 250, 450)]]

#its <- sample(1:500,3)
mp_metrics <- lapply(mp_metrics,function(x) window(x,start = 2024))
# F

ggsave("report/output/metricsFTCMP_Mai2024_lag.png",
plotOMruns(om_metrics$F, FLQuants(lapply(mp_metrics, "[[", "F")), iter=its , ylim = c(0,1.5))
)

# SSB

ggsave("report/output/metricsSBTCMP_Mai2024_lag.png",
plotOMruns(om_metrics$SB, FLQuants(lapply(mp_metrics, "[[", "SB")), iter=its)
)

# CATCH

ggsave("report/output/metricsCTCMP_Mai2024_lag.png",
plotOMruns(om_metrics$C, FLQuants(lapply(mp_metrics, "[[", "C")), iter=its)
)


# SUMMARY table

perfSum <- perf[
  statistic == "PSBMSY" & year == 'MT' |
  statistic == "PSBlim" & year == 'MT' |
  statistic == "SBMSY" & year == 'MT' |
  statistic == 'green' & year == 'tune' |
  statistic == 'CMSY' & year == 'MT' |
  statistic == 'maxTACup' & year == 'MT' |
  statistic == 'maxTACdown' & year == 'MT' |
  statistic == 'nTACchange' & year == 'MT' |
  statistic == 'IACC' & year == 'tac' |
  statistic == 'C' & year == 'MT' , ]



print(summTable(perfSum,statistics=c("C",'nTACchange',"IACC","PSBMSY","PSBlim")), type="html", file="report/output/summary_tableTCMP_Mai2024_lag.html")
print(summTable(perfSum,statistics=c("C",'nTACchange',"IACC","PSBMSY","PSBlim")), type="latex", file="report/output/summary_tableTCMP_Mai2024_lag.png")

   data<-perfSum
    stts<-c("C",'nTACchange',"IACC","PSBMSY","PSBlim") 
    probs<-c(0.1, 0.5, 0.9)
   # data <- data[statistic %in% stts, ]
    data[, `:=`(name, foo(name))]
    qdata <- data[, as.list(quantile(data, probs = probs, na.rm = TRUE)),
        keyby = list(statistic, name, mp)]
    qdata$"50%"[qdata$statistic=="green"]     <-       round(aggregate(data~mp ,subset(data,statistic=="green") ,mean)$data,2)
    qdata[, `:=`(fig, paste0(format(round(`50%`, 2), digits = 3,
        scientific = FALSE, trim = TRUE), " (", format(round(`10%`,
        2), digits = 1, trim = TRUE, scientific = FALSE), "-",
        format(round(`90%`, 2), digits = 1, trim = TRUE, scientific = FALSE),
        ")"))]
    qtab <- dcast(qdata, mp ~ name, value.var = "fig")


fwrite(qtab,"report/output/custom_summary_tableTCMP_Mai2024_lag.txt", sep=";",col.names = T, row.names = FALSE)


# FINAL table

print(
resTable(perf[year == "ST", ], statistics=statistics),
type="html", file="report/output/full_table_ST_TCMP_Mai2024_lag.html")

print(
resTable(perf[year == "MT", ], statistics=statistics),
type="html", file="report/output/full_table_MT_TCMP_Mai2024_lag.html")

print(
resTable(perf[year == "LT", ], statistics=statistics),
type="html", file="report/output/full_table_LT_TCMP_Mai2024_lag.html")

# EXTRA

# DIFF mean ~ median Kobe(green)
# hist(perf[mp=='MP3' & statistic=='green',]$data)

# }}}


















