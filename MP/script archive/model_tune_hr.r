# model_tune.R - DESC
# SWO/MP/model_tune_perfect.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


source("model.R")
#load_all('~/Active/MSE_PKG@flr/mse')

library(doParallel)
registerDoParallel(50)

# SET output folder
outpath <- "model/hr"
# load the hcr function if not in the library yet
source("hr.hcr.R")


### HCR configuration

#### ITRIGGER 
#    choice of trigger values for the index
#    that is the trigger point for the  index, below which you reduce the HR applied
#    in Fisher et al, 2022, the suggest Iloss * buffer

jpll<- index(observations(oem)$idx[[1]])
ggsave((plot(unitSums(ssb(om))/refpts(om)$SBMSY) + 
    ggtitle("Sb/SBmsy") + 
    xlim(1994,2018)  +
    geom_hline(yintercept = 1) +
plot(jpll) + 
    ggtitle("JPLL_NW")) , file = file.path(outpath,"cpue_vs_status.png"))

#it seems when the index was at its lowests, the stock was generally above SBmsy

# apply the default approach then :
# historic min
itrigger <-  min(index(observations(oem)$idx[[1]]),na.rm=T)
# buffer : default = 1.4
itrigger <- 1.4 * itrigger   # 0.8

### HR TARGET
#   HR to be appied (when no biomass safeguard is applied)
#   approach Fisher et al 2022: look at HR for year when F<Fmsy
#   here we can use the OM to define years when we are in the green part of the Kobe plot 
pgreen <- performance(om, metrics=mets[c("SB", "F")], statistic=statistics["green"] ,years=ac(1994:2018))
pgreen <- aggregate(data ~ year, pgreen , median)
names(pgreen)[2] <- "pgreen" 
pgreen$pgreen <- round(pgreen$pgreen,0)
histHR <- as.data.frame(iterMeans(unitSums(catch(om))[,ac(1994:2018)]%/%index(observations(oem)$idx[[1]])[,ac(1994:2018)]),drop=T)
histHR <- merge(histHR,pgreen,all.x=T)
targHR<- mean(histHR$data[histHR$pgreen==1])

ggsave(
ggplot(histHR , aes(year,data)) + geom_line() + geom_point(aes(col=factor(pgreen)),size=2)   + ylab("relative harvest rate") + geom_hline(yintercept = targHR),
file=file.path(outpath,"targetHR.png"))






# CONTROL
control <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind),
  hcr = mseCtrl(method=hr.hcr, args=list(trigger=itrigger,
    target=targHR, lambda=1, metric="mean"))))
    
perf_hr <- mp(om, oem=oem, ctrl=control, args=mseargs, parallel=TRUE)

save(perf_hr,file=file.path(outpath,"hr_default.RData"))
load(file.path(outpath,"hr_default.RData"))
plot(perf_hr)




# P(kobe=green) = 0.5
hr_05_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(20000, 60000)), prob=0.5, tol=0.02, maxit=12, parallel=TRUE,
  tracking=c("tac.hcr"))
  
### funny enough, the target found is very close to the empirical one!  

saveRDS(hr_05_targ, file=file.path(outpath, "hr_05_targ.Rds"),
  compress="xz")

rm(hr_05_targ)

# P(kobe=green) = 0.6   target = 27500
hr_06_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(10000, 30000)), prob=0.6, tol=0.02, maxit=12, parallel=TRUE,
  tracking=c("tac.hcr"))

saveRDS(hr_06_targ, file=file.path(outpath, "hr_06_targ.Rds"),
  compress="xz")

rm(hr_06_targ)


# P(kobe=green) = 0.7
hr_07_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(10000, 30000)), prob=0.7, tol=0.02, maxit=12, parallel=TRUE,
  tracking=c("tac.hcr"))
saveRDS(hr_07_targ, file=file.path(outpath, "hr_07_targ.Rds"),
  compress="xz")

rm(hr_07_targ)


# FINAL list

files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)
tune <- lapply(setNames(files, gsub("\\.Rds$", "", basename(files))), readRDS)

# EXTRACT metrics & performance
stats <-statistics[c("SBMSY","green","risk3","C","IACC")]

perf <- performance(tune, metrics=mets, statistics=stats, years=tperiod)

summ <- lapply(tune, function(x) metrics(stock(om(x)), mets))

omsumm <- metrics(stock(om), mets)

save(perf, summ, omsumm, file="model/tune_hr.Rdata", compress="xz")
# save(tune, file="model/tune_cpue.Rdata", compress="xz")


load("model/tune_hr.Rdata")

# MP colors: red, green and blue, 3 hues
cols <- c("#E53116", "#FF593E", "#FF8166",
 "#3B9812", "#63c03a", "#8be862",
 "#096D98", "#3195c0", "#59bde8")

library(mseviz)
 
ggsave(file.path(outpath,"perfHR.png"),
plotBPs(perf, statistics=c("SBMSY", "green", "PSBlim", "C", "IACC")) +
  ylim(c(0,NA)) +
  scale_fill_discrete(type=cols) +
  theme(legend.position = c(.85, .2))
)
 