  # model_tune.R - DESC
# SWO/MP/model_tune_perfect.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


source("model.R")
#load_all('~/Active/MSE_PKG@flr/mse')

library(doParallel)
registerDoParallel(30)

# SET output folder
outpath <- "model/cpue"




#cpue.hcr <-mse::cpue.hcr


#### TUNNING MPs ##  +15/-15IAV : #######################################


# CONTROL ------------

## CPUE slow with k12=0.1 and k34=0.3    k parameteres values were determined by inspecting a grid (see presentations TCMP 2023 and others)
controlSlow <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=0.1, k2=0.1, k3=0.3, k4=0.3, dlow = 0.85, dupp = 1.15, target=0.7))))

## CPUE fast with 
# k parameteres values were determined by inspecting a grid (see presentations TCMP 2023 and others)
controlFast <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=2.1, k2=2.1, k3=1.2, k4=1.2, dlow = 0.85, dupp = 1.15, target=0.7))))


# tune P(kobe=green) = 0.5
cpueSlow_05_targ1515IAV <- tunebisect(om, oem=oem, control=controlSlow, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.5, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueSlow_05_targ1515IAV, file=file.path(outpath, "cpueSlow_05_targ1515IAV.Rds"),
  compress="xz")

rm(cpueSlow_05_targ1515IAV)

cpueFast_05_targ1515IAV <- tunebisect(om, oem=oem, control=controlFast, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.5, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueFast_05_targ1515IAV, file=file.path(outpath, "cpueFast_05_targ1515IAV.Rds"),
  compress="xz")

rm(cpueFast_05_targ1515IAV)



# P(kobe=green) = 0.6
cpueSlow_06_targ1515IAV <- tunebisect(om, oem=oem, control=controlSlow, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.6, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueSlow_06_targ1515IAV, file=file.path(outpath, "cpueSlow_06_targ1515IAV.Rds"),
  compress="xz")

rm(cpueSlow_06_targ1515IAV)

cpueFast_06_targ1515IAV <- tunebisect(om, oem=oem, control=controlFast, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.6, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueFast_06_targ1515IAV, file=file.path(outpath, "cpueFast_06_targ1515IAV.Rds"),
  compress="xz")

rm(cpueFast_06_targ1515IAV)


# P(kobe=green) = 0.7
cpueSlow_07_targ1515IAV <- tunebisect(om, oem=oem, control=controlSlow, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.7, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueSlow_07_targ1515IAV, file=file.path(outpath, "cpueSlow_07_targ1515IAV.Rds"),
  compress="xz")

rm(cpueSlow_07_targ1515IAV)

cpueFast_07_targ1515IAV <- tunebisect(om, oem=oem, control=controlFast, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.7, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueFast_07_targ1515IAV, file=file.path(outpath, "cpueFast_07_targ1515IAV.Rds"),
  compress="xz")

rm(cpueFast_07_targ1515IAV)




#### TUNNING MPs ##  +10/-10IAV : ONLY FOR P(GREEN) = 60%  #######################################


# CONTROL ------------

## CPUE slow with k12=0.1 and k34=0.3    k parameteres values were determined by inspecting a grid (see presentations TCMP 2023 and others)
controlSlow <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=0.1, k2=0.1, k3=0.3, k4=0.3, dlow = 0.90, dupp = 1.10, target=0.7))))

## CPUE fast with 
# k parameteres values were determined by inspecting a grid (see presentations TCMP 2023 and others)
controlFast <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=2.1, k2=2.1, k3=1.2, k4=1.2, dlow = 0.90, dupp = 1.10, target=0.7))))

# P(kobe=green) = 0.6
cpueSlow_06_targ1010IAV <- tunebisect(om, oem=oem, control=controlSlow, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.6, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueSlow_06_targ1010IAV, file=file.path(outpath, "cpueSlow_06_targ1010IAV.Rds"),
  compress="xz")

rm(cpueSlow_06_targ1010IAV)

cpueFast_06_targ1010IAV <- tunebisect(om, oem=oem, control=controlFast, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.6, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueFast_06_targ1010IAV, file=file.path(outpath, "cpueFast_06_targ1010IAV.Rds"),
  compress="xz")

rm(cpueFast_06_targ1010IAV)


#### TUNNING MPs ##  +15/-10IAV : ONLY FOR P(GREEN) = 60%  #######################################


# CONTROL ------------

## CPUE slow with k12=0.1 and k34=0.3    k parameteres values were determined by inspecting a grid (see presentations TCMP 2023 and others)
controlSlow <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=0.1, k2=0.1, k3=0.3, k4=0.3, dlow = 0.90, dupp = 1.15, target=0.7))))

## CPUE fast with 
# k parameteres values were determined by inspecting a grid (see presentations TCMP 2023 and others)
controlFast <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=2.1, k2=2.1, k3=1.2, k4=1.2, dlow = 0.90, dupp = 1.15, target=0.7))))

# P(kobe=green) = 0.6
cpueSlow_06_targ1510IAV <- tunebisect(om, oem=oem, control=controlSlow, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.6, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueSlow_06_targ1510IAV, file=file.path(outpath, "cpueSlow_06_targ1510IAV.Rds"),
  compress="xz")

rm(cpueSlow_06_targ1510IAV)

cpueFast_06_targ1510IAV <- tunebisect(om, oem=oem, control=controlFast, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.6, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueFast_06_targ1510IAV, file=file.path(outpath, "cpueFast_06_targ1510IAV.Rds"),
  compress="xz")

rm(cpueFast_06_targ1510IAV)


# P(kobe=green) = 0.7
cpueSlow_07_targ1510IAV <- tunebisect(om, oem=oem, control=controlSlow, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.7, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueSlow_07_targ1510IAV, file=file.path(outpath, "cpueSlow_07_targ1510IAV.Rds"),
  compress="xz")

rm(cpueSlow_07_targ1510IAV)

cpueFast_07_targ1510IAV <- tunebisect(om, oem=oem, control=controlFast, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.7, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpueFast_07_targ1510IAV, file=file.path(outpath, "cpueFast_07_targ1510IAV.Rds"),
  compress="xz")

rm(cpueFast_07_targ1510IAV)



#### robustness test to a 10% overshoot consistent
outpath2 <- "model/test"

iem <- FLiem(method=noise.iem,  args=list(noise=unitMeans(rec(om)) %=% (1.1)))


cpueSlow_06_targ1510IAV<-readRDS(file.path(outpath, "cpueSlow_06_targ1510IAV.Rds"))

cpueSlow_06_targ1510IAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = cpueSlow_06_targ1510IAV@control, args = cpueSlow_06_targ1510IAV@args, parallel= TRUE)
saveRDS(cpueSlow_06_targ1510IAV_IE, file=file.path(outpath2, "cpueSlow_06_targ1510IAV_IE.Rds"),  compress="xz")



cpueFast_06_targ1510IAV<-readRDS(file.path(outpath, "cpueFast_06_targ1510IAV.Rds"))

cpueFast_06_targ1510IAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = cpueFast_06_targ1510IAV@control, args = cpueFast_06_targ1510IAV@args, parallel= TRUE)
saveRDS(cpueFast_06_targ1510IAV_IE, file=file.path(outpath2, "cpueFast_06_targ1510IAV_IE.Rds"),  compress="xz")


cpueSlow_07_targ1510IAV<-readRDS(file.path(outpath, "cpueSlow_07_targ1510IAV.Rds"))

cpueSlow_07_targ1510IAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = cpueSlow_07_targ1510IAV@control, args = cpueSlow_07_targ1510IAV@args, parallel= TRUE)
saveRDS(cpueSlow_07_targ1510IAV_IE, file=file.path(outpath2, "cpueSlow_07_targ1510IAV_IE.Rds"),  compress="xz")


cpueFast_07_targ1510IAV<-readRDS(file.path(outpath, "cpueFast_07_targ1510IAV.Rds"))

cpueFast_07_targ1510IAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = cpueFast_07_targ1510IAV@control, args = cpueFast_07_targ1510IAV@args, parallel= TRUE)
saveRDS(cpueFast_07_targ1510IAV_IE, file=file.path(outpath2, "cpueFast_07_targ1510IAV_IE.Rds"),  compress="xz")



#### robustness test to a 2 year management lag

outpath2 <- "model/test"

# first need to feel in some catch in the OM for 2024...
ctrlIY<- fwdControl(year=2024, relYear=2023, quant="fbar", value=1, minAge=2, maxAge=8)

om2 <- fwd(om,control=ctrlIY)


# SUBSAMPLE for testing
#idx <- sample(seq(500), 2)
#oms <- iter(om2,  idx)
#oems <- iter(oem, idx)
#

cpueSlow_06_targ1510IAV<-readRDS(file.path(outpath, "cpueSlow_06_targ1510IAV.Rds"))
cpueSlow_06_targ1510IAV@control$hcr@method  <- cpue.hcr
mpargs2<-cpueSlow_06_targ1510IAV@args
mpargs2$management_lag  <- 2

cpueSlow_06_targ1510IAV_lag<- mp (om = om2, oem = oem, , ctrl = cpueSlow_06_targ1510IAV@control, args = mpargs2, parallel= TRUE)
saveRDS(cpueSlow_06_targ1510IAV_lag, file=file.path(outpath2, "cpueSlow_06_targ1510IAV_lag.Rds"),  compress="xz")


cpueSlow_07_targ1510IAV<-readRDS(file.path(outpath, "cpueSlow_07_targ1510IAV.Rds"))
cpueSlow_07_targ1510IAV@control$hcr@method  <- cpue.hcr
mpargs2<-cpueSlow_07_targ1510IAV@args
mpargs2$management_lag  <- 2

cpueSlow_07_targ1510IAV_lag<- mp (om = om2, oem = oem, , ctrl = cpueSlow_07_targ1510IAV@control, args = mpargs2, parallel= TRUE)
saveRDS(cpueSlow_07_targ1510IAV_lag, file=file.path(outpath2, "cpueSlow_07_targ1510IAV_lag.Rds"),  compress="xz")



cpueFast_06_targ1510IAV<-readRDS(file.path(outpath, "cpueFast_06_targ1510IAV.Rds"))
cpueFast_06_targ1510IAV@control$hcr@method  <- cpue.hcr
mpargs2<-cpueFast_06_targ1510IAV@args
mpargs2$management_lag  <- 2

cpueFast_06_targ1510IAV_lag<- mp (om = om2, oem = oem, , ctrl = cpueFast_06_targ1510IAV@control, args = mpargs2, parallel= TRUE)
saveRDS(cpueFast_06_targ1510IAV_lag, file=file.path(outpath2, "cpueFast_06_targ1510IAV_lag.Rds"),  compress="xz")


cpueFast_07_targ1510IAV<-readRDS(file.path(outpath, "cpueFast_07_targ1510IAV.Rds"))
cpueFast_07_targ1510IAV@control$hcr@method  <- cpue.hcr
mpargs2<-cpueFast_07_targ1510IAV@args
mpargs2$management_lag  <- 2

cpueFast_07_targ1510IAV_lag<- mp (om = om2, oem = oem, , ctrl = cpueFast_07_targ1510IAV@control, args = mpargs2, parallel= TRUE)
saveRDS(cpueFast_07_targ1510IAV_lag, file=file.path(outpath2, "cpueFast_07_targ1510IAV_lag.Rds"),  compress="xz")
