# test.R - DESC
# /home/mosquia/Backlog/SWO_MSE@iotc/swo/MP/test.R

# Copyright (c) WUR, 2022.
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2

source("model.R")

 ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 
# robustness test to implementation error
############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 


# msake sure that the cpue.hcr used i=s the correct one
cpue.hcr <- function (stk, ind, k1, k2, k3, k4, target = 1, dlow = 0.85, 
    dupp = 1.15, args, tracking) 
{
    ay <- args$ay
    frq <- args$frq
    man_lag <- args$management_lag
    slope <- ind$slope
    mcpue <- ind$mean
    ka <- ifelse(slope > 0, k1, k2)
    kb <- ifelse(mcpue > target, k3, k4)
    prev <- tracking[[1]]["hcr", ac(ay)]
    if (all(is.na(prev))) 
    prev <- seasonSums(catch(stk)[, ac(ay - args$data_lag)])
    
    mult <-  (1 + ka * slope + kb * (mcpue - target))
    if (!is.na(dupp)) 
        mult[mult >  dupp] <-  dupp
    if (!is.na(dlow)) 
        mult[mult <  dlow] <-  dlow
           
    tac <- mult * prev   
    ctrl <- fwdControl(c(lapply(seq(ay + man_lag, ay + frq), 
        function(x) list(quant = "catch", value = c(tac), year = x))))
    return(list(ctrl = ctrl, tracking = tracking))
}


# load the tuned MPs
outpath <- file.path("model", c("cpue"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)
files <- files[grep("targIAV",files)]


tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))), readRDS)

outpath2 <- "model/test"

# alter recruitment deviates in the OM
om2<-om
residuals(om2@sr)[,ac(2022:2025)]  <-   0.1     # recruitment is 30% of the SR model prediction)



cpue_05_targIAV_Rfail<- mp (om = om2, oem = oem,  ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
saveRDS(cpue_05_targIAV_Rfail, file=file.path(outpath2, "cpue_05_targIAV_Rfail.Rds"),  compress="xz")


cpue_06_targIAV_Rfail<- mp (om = om2, oem = oem, ctrl = tuned[[2]]@control, args = tuned[[2]]@args, parallel= TRUE)
saveRDS(cpue_06_targIAV_Rfail, file=file.path(outpath2, "cpue_06_targIAV_Rfail.Rds"),  compress="xz")


cpue_07_targIAV_Rfail<- mp (om = om2, oem = oem, ctrl = tuned[[3]]@control, args = tuned[[3]]@args, parallel= TRUE)
saveRDS(cpue_07_targIAV_Rfail, file=file.path(outpath2, "cpue_07_targIAV_Rfail.Rds"),  compress="xz")





