
# test.R - DESC
# /home/mosquia/Backlog/SWO_MSE@iotc/swo/MP/test.R

# Copyright (c) WUR, 2022.
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2

source("model.R")

library(doParallel)
registerDoParallel(50)


############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 
# robustness test to a recruitment failure
############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 


# alter recruitment deviates in the OM
om2<-om
residuals(om2@sr)[,ac(2022:2025)]  <-   0.1     # recruitment is 30% of the SR model prediction)

# test with F=Fmsy
ctrlFmsy<- fwdControl(list(year=2022:2042,  # projection for fishery
                              quant="fbar",
                              value= refpts(om)$FMSY))

projFmsy  <- fwd(om,control=ctrlFmsy)
projFmsy2 <- fwd(om2,control=ctrlFmsy)

plot(projFmsy) + plot(projFmsy2)

perf <- performance(projFmsy, ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)
                      

perf2 <- performance(projFmsy2, ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf$run   <- "base"
perf2$run  <- "Rfail"

perf <- rbind(perf, perf2)


pldt <- aggregate( data~year + run + statistic, perf, mean)

ggplot(pldt , aes(as.numeric(year),data)) +  
    geom_line(aes(colour=run)) + 
    facet_grid(statistic~. ,scale = "free")
 


 ### now with CPUE MP
control.slow <- mpCtrl(list(
    est = mseCtrl(method=cpue.ind, args=list(index=1)),
    hcr = mseCtrl(method=cpue.hcr, args=list(k1=0.1, k2=0.1, k3=0.41, k4=0.41, target=1.09))))
      

control.fast <- mpCtrl(list(
    est = mseCtrl(method=cpue.ind, args=list(index=1)),
    hcr = mseCtrl(method=cpue.hcr, args=list(k1=2.5, k2=2.5, k3=0.57, k4=0.57, target= 1.21))))


perf_cpue_slow_base <- mp(om, oem=oem, ctrl=control.slow, args=mseargs, parallel=TRUE)
perf_cpue_slow_Rfail <- mp(om2, oem=oem, ctrl=control.slow, args=mseargs, parallel=TRUE)  

perf_cpue_fast_base <- mp(om, oem=oem, ctrl=control.fast, args=mseargs, parallel=TRUE)
perf_cpue_fast_Rfail <- mp(om2, oem=oem, ctrl=control.fast, args=mseargs, parallel=TRUE)  


perf_cpue_slow_base <- performance(perf_cpue_slow_base ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf_cpue_slow_base$sc <- "base"  
perf_cpue_slow_base$MP <- "slow"  

perf_cpue_slow_Rfail <- performance(perf_cpue_slow_Rfail ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf_cpue_slow_Rfail$sc <- "Rfail"  
perf_cpue_slow_Rfail$MP <- "slow"


perf_cpue_fast_base <- performance(perf_cpue_fast_base ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf_cpue_fast_base$sc <- "base"  
perf_cpue_fast_base$MP <- "fast"  

perf_cpue_fast_Rfail <- performance(perf_cpue_fast_Rfail ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf_cpue_fast_Rfail$sc <- "Rfail"  
perf_cpue_fast_Rfail$MP <- "fast"



save(perf_cpue_slow_base,perf_cpue_slow_Rfail,perf_cpue_fast_base,perf_cpue_fast_Rfail,file="restest.RData")
load("restest.RData")





# agein but with TAC stabiliser on


#### quick fix for the fact that TAC var limits are not in the MP function : just redefine it here
cpue.hcr <- function (stk, ind, k1, k2, k3, k4, target = 1, dlow = 0.85, 
    dupp = 1.15, args, tracking) 
{
    ay <- args$ay
    frq <- args$frq
    man_lag <- args$management_lag
    slope <- ind$slope
    mcpue <- ind$mean
    ka <- ifelse(slope > 0, k1, k2)
    kb <- ifelse(mcpue > target, k3, k4)
    prev <- tracking[[1]]["hcr", ac(ay)]
    if (all(is.na(prev))) 
    prev <- seasonSums(catch(stk)[, ac(ay - args$data_lag)])
    
    mult <-  (1 + ka * slope + kb * (mcpue - target))
    if (!is.na(dupp)) 
        mult[mult >  dupp] <-  dupp
    if (!is.na(dlow)) 
        mult[mult <  dlow] <-  dlow
           
    tac <- mult * prev   
    ctrl <- fwdControl(c(lapply(seq(ay + man_lag, ay + frq), 
        function(x) list(quant = "catch", value = c(tac), year = x))))
    return(list(ctrl = ctrl, tracking = tracking))
}

#cpue.hcr <-mse::cpue.hcr
    
control.slow <- mpCtrl(list(
    est = mseCtrl(method=cpue.ind, args=list(index=1)),
    hcr = mseCtrl(method=cpue.hcr, args=list(k1=0.1, k2=0.1, k3=0.3357143, k4=0.3357143, target=0.7))))
      

control.fast <- mpCtrl(list(
    est = mseCtrl(method=cpue.ind, args=list(index=1)),
    hcr = mseCtrl(method=cpue.hcr, args=list(k1=2.1, k2=2.1, k3=1.1214286, k4=1.1214286, target=  0.7078125))))


perf_cpue_slow_baseIAV <- mp(om, oem=oem, ctrl=control.slow, args=mseargs, parallel=TRUE)
perf_cpue_slow_RfailIAV <- mp(om2, oem=oem, ctrl=control.slow, args=mseargs, parallel=TRUE)  


perf_cpue_fast_baseIAV <- mp(om, oem=oem, ctrl=control.fast, args=mseargs, parallel=TRUE)
perf_cpue_fast_RfailIAV <- mp(om2, oem=oem, ctrl=control.fast, args=mseargs, parallel=TRUE)  


perf_cpue_slow_baseIAV <- performance(perf_cpue_slow_baseIAV ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf_cpue_slow_baseIAV$sc <- "base"  
perf_cpue_slow_baseIAV$MP <- "slow"  

perf_cpue_slow_RfailIAV <- performance(perf_cpue_slow_RfailIAV ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf_cpue_slow_RfailIAV$sc <- "Rfail"  
perf_cpue_slow_RfailIAV$MP <- "slow"


perf_cpue_fast_baseIAV <- performance(perf_cpue_fast_baseIAV ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf_cpue_fast_baseIAV$sc <- "base"  
perf_cpue_fast_baseIAV$MP <- "fast"  

perf_cpue_fast_RfailIAV <- performance(perf_cpue_fast_RfailIAV ,metrics= mets[c("SB","F")],
                      statistics=statistics[c("green","risk3")] , 
                      refpts=refpts(om),
                      years = 2022:2042)

perf_cpue_fast_RfailIAV$sc <- "Rfail"  
perf_cpue_fast_RfailIAV$MP <- "fast"



save(perf_cpue_slow_baseIAV,perf_cpue_slow_RfailIAV,perf_cpue_fast_baseIAV,perf_cpue_fast_RfailIAV,file="restestIAV.RData")



load("restest.RData")
perfs <- do.call(rbind,list(perf_cpue_slow_base,perf_cpue_slow_Rfail,perf_cpue_fast_base,perf_cpue_fast_Rfail))
perfs$TACstab <-"off"

load("restestIAV.RData")
perfsIAV <- do.call(rbind,list(perf_cpue_slow_baseIAV,perf_cpue_slow_RfailIAV,perf_cpue_fast_baseIAV,perf_cpue_fast_RfailIAV))
perfsIAV$TACstab <-"on"


perfs<-rbind(perfs,perfsIAV)

pldt <- aggregate( data~year + sc + MP + statistic + TACstab, perfs, mean)

ggsave ( "./model/cpue/robustness.png",
ggplot(pldt , aes(as.numeric(year),data)) +  
    geom_line(aes(colour=sc,linetype = MP)) +
    xlab("year")   +
    ylab("prob")   + 
    facet_grid(statistic~TACstab ,scale = "free")  )












 ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 
# robustness test to implementation error
############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 


# msake sure that the cpue.hcr used i=s the correct one
cpue.hcr <- function (stk, ind, k1, k2, k3, k4, target = 1, dlow = 0.85, 
    dupp = 1.15, args, tracking) 
{
    ay <- args$ay
    frq <- args$frq
    man_lag <- args$management_lag
    slope <- ind$slope
    mcpue <- ind$mean
    ka <- ifelse(slope > 0, k1, k2)
    kb <- ifelse(mcpue > target, k3, k4)
    prev <- tracking[[1]]["hcr", ac(ay)]
    if (all(is.na(prev))) 
    prev <- seasonSums(catch(stk)[, ac(ay - args$data_lag)])
    
    mult <-  (1 + ka * slope + kb * (mcpue - target))
    if (!is.na(dupp)) 
        mult[mult >  dupp] <-  dupp
    if (!is.na(dlow)) 
        mult[mult <  dlow] <-  dlow
           
    tac <- mult * prev   
    ctrl <- fwdControl(c(lapply(seq(ay + man_lag, ay + frq), 
        function(x) list(quant = "catch", value = c(tac), year = x))))
    return(list(ctrl = ctrl, tracking = tracking))
}


# load the Jabba based tuned MPs
outpath <- file.path("model", c("cpue"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)
files <- files[grep("targIAV",files)]


tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))), readRDS)

outpath2 <- "model/test"

iem <- FLiem(method=noise.iem,
  args=list(noise=unitMeans(rec(om)) %=% (1.2)))



test<- mp (om = om, oem ,  ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)

cpue_05_targIAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
saveRDS(cpue_05_targIAV_IE, file=file.path(outpath2, "cpue_05_targIAV_IE"),  compress="xz")


cpue_06_targIAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[2]]@control, args = tuned[[2]]@args, parallel= TRUE)
saveRDS(cpue_06_targIAV_IE, file=file.path(outpath2, "cpue_06_targIAV_IE"),  compress="xz")


cpue_07_targIAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
saveRDS(cpue_07_targIAV_IE, file=file.path(outpath2, "cpue_07_targIAV_IE"),  compress="xz")





