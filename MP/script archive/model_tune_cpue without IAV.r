

# P(kobe=green) = 0.5
subset(cuales , pGreen=="0.5" & strategy== "slow" & type == "symmetric" )
# CONTROL
control <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=0.1, k2=0.1, k3=0.3357143, k4=0.3357143, dlow = NA, dupp = NA, target=0.6896552))))

# P(kobe=green) = 0.5
cpue_05_targIAV <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.2, 1)), prob=0.5, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpue_05_targIAV, file=file.path(outpath, "cpue_05_targnoIAV.Rds"),
  compress="xz")

rm(cpue_05_targIAV)



# P(kobe=green) = 0.6
subset(cuales , pGreen=="0.6" & strategy== "slow" & type == "symmetric" )
# CONTROL
control <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=0.1, k2=0.1, k3=0.3357143, k4=0.3357143, dlow = NA, dupp = NA, target=0.6))))

cpue_06_targIAV <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.5, 1.1)), prob=0.6, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpue_06_targIAV, file=file.path(outpath, "cpue_06_targnoIAV.Rds"),
  compress="xz")

rm(cpue_06_targIAV)







# P(kobe=green) = 0.7
subset(cuales , pGreen=="0.7" & strategy== "slow" & type == "symmetric" )
# CONTROL
control <- mpCtrl(list(
  est = mseCtrl(method=cpue.ind, args=list(index=1)),
  hcr = mseCtrl(method=cpue.hcr,
    args=list(k1=0.1, k2=0.1, k3=0.3357143, k4=0.3357143, dlow = NA, dupp = NA, target=0.6))))

cpue_07_targIAV <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=c(0.6, 1.3)), prob=0.7, tol=0.01, maxit=12, parallel=TRUE,
  tracking=c("cpue.ind", "slope.ind"))

saveRDS(cpue_07_targIAV, file=file.path(outpath, "cpue_07_targnoIAV.Rds"),
  compress="xz")

rm(cpue_07_targIAV)
#



