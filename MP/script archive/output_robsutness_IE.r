# output.R - PREPARE mode results: metrics, performance
# swo/MP/output.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)
library(mseviz)

source("utilities.R")
#source("utilities old.R")

# LOAD statistics & OM

source("model.R")

# CHOOSE IOTC stats

stats <- statistics[c("SB0", "minSB0", "SBMSY", "Ftarget", "FMSY",
  "green", "red", "PSB20B0", "PSBlim", "C", "CMSY", "IACC", "PC0")]

# TPERIOD, last OM year + 11:15

tperiod <- list(2034:2038)

# MPs list

#outpath <- file.path("model", c("jabba", "hr", "cpue"))
outpath <- file.path("model", c("jabba",  "cpue","test"))

files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)

files <- files[grep("IE",files)]

files <- files[-c(5,7,9)]

# SELECT runs

tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))),
  readRDS)

# RENAME MPs

nms <- setNames(names(tuned),  nm=paste0("MP", seq(length(files))))

names(tuned) <- names(nms)

# EXTRACT metrics

mp_metrics <- lapply(tuned, function(x) metrics(stock(x), metrics=mets))

om_metrics <- FLQuants(metrics(window(om, end=2022), metrics=mets))

# COMPUTE performance

perf <- performance(tuned, metrics=mets, statistics=stats, years=tperiod)

year_perf <- performance(tuned, metrics=mets, statistics=stats,
  years=list(seq(2022, length=5), seq(2022, length=10), seq(2022, length=19)))

kobe_perf <- performance(tuned, metrics=mets, statistics=kobestatistics,
  years=2022:2040)

# MERGE results & perf
results[, iter := as.character(iter)]
perfres <- merge(perf, results, by='iter')

# SAVE

save(mp_metrics, om_metrics, perf, year_perf, kobe_perf, perfres, nms,
  file="output/output_robust_IE.2.Rdata", compress="xz")



