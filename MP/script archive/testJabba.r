# test.R - DESC
# /home/mosquia/Backlog/SWO_MSE@iotc/swo/MP/test.R

# Copyright (c) WUR, 2022.
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2




source("model.R")

library(doFuture)
plan(multicore, workers=15)
options(future.globals.maxSize=629145600)


############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 
# robustness test to a recruitment failure
############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 


# alter recruitment deviates in the OM
om2<-om
residuals(om2@sr)[,ac(2022:2025)]  <-   0.1     # recruitment is 30% of the SR model prediction)




# load the Jabba based tuned MPs
outpath <- file.path("model", c("jabba"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)

tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))), readRDS)

outpath2 <- "model/test"



test<- mp (om = iter(om,1:10), oem = iter(oem,1:10),  ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
test<- mp (om = om, oem = oem,  ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)

#save(test, file="testJabba.RData")
testargs <- tuned[[1]]@args
args$fy <-2031
test_Rfail<- mp (om = iter(om2,1:5), oem = iter(oem,1:5), ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
pdf("test.pdf")
plot(test_Rfail)
dev.off()

jabba_hcst_05_targ_Rfail<- mp (om = om2, oem = oem, ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
saveRDS(jabba_hcst_05_targ_Rfail, file=file.path(outpath2, "jabba_hcst_05_targ_Rfail.Rds"),  compress="xz")


jabba_hcst_06_targ_Rfail<- mp (om = om2, oem = oem, ctrl = tuned[[2]]@control, args = tuned[[2]]@args, parallel= TRUE)
saveRDS(jabba_hcst_06_targ_Rfail, file=file.path(outpath2, "jabba_hcst_06_targ_Rfail.Rds"),  compress="xz")


jabba_hcst_07_targ_Rfail<- mp (om = om2, oem = oem, ctrl = tuned[[3]]@control, args = tuned[[3]]@args, parallel= TRUE)
saveRDS(jabba_hcst_07_targ_Rfail, file=file.path(outpath2, "jabba_hcst_07_targ_Rfail.Rds"),  compress="xz")


############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 
# robustness test to implementation error
############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 


iem <- FLiem(method=noise.iem,  args=list(noise=unitMeans(rec(om)) %=% (1.2)))


test<- mp (om = iter(om,1:10), oem = iter(oem,1:10), iem =iter(iem,1:10), ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)




jabba_hcst_05_targ  <- tuned[[1]] 
jabba_hcst_05_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
saveRDS(jabba_hcst_05_targ_IE, file=file.path(outpath2, "jabba_hcst_05_targ_IE.2.Rds"),  compress="xz")

jabba_hcst_06_targ  <- tuned[[2]] 
jabba_hcst_06_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[2]]@control, args = tuned[[2]]@args, parallel= TRUE)
saveRDS(jabba_hcst_06_targ_IE, file=file.path(outpath2, "jabba_hcst_06_targ_IE.2.Rds"),  compress="xz")

jabba_hcst_07_targ  <- tuned[[3]] 
jabba_hcst_07_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[3]]@control, args = tuned[[3]]@args, parallel= TRUE)
saveRDS(jabba_hcst_07_targ_IE, file=file.path(outpath2, "jabba_hcst_07_targ_IE.2.Rds"),  compress="xz")


#### check what happens with a perfect SA?
#
#ctr  <- tuned[[2]]@control 
#ctr$est
#
#jabba_hcst_06_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = expl@control, args = tuned[[2]]@args, parallel= TRUE)
#saveRDS(jabba_hcst_06_targ_IE, file=file.path(outpath2, "jabba_hcst_06_targ_IE.Rds"),  compress="xz")
#
#
#