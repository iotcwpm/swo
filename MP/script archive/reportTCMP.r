# report.R - DESC
# /report.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)
library(mseviz)
library(metR)

source("model.R")



# --- OUTPUT for the main plots 

load("output/outputTCMP.Rdata")

# MP colors: red, green and blue, 3 hues

nms    <- data.frame(mp=names(nms),nms)
perf<-merge(perf,nms,all.x=T)
perf$mp<-perf$nms

cols <- c("#E53116", "#FF593E", "#FF8166",
 "#3B9812", "#63c03a", "#8be862" )# ,
 "#096D98", "#3195c0", "#59bde8")

# plotBPs

ggsave("report/output/bpsTCMP.png",
plotBPs(perf, statistics=c("SBMSY", "green", "PSBlim", "C", "IACC"),show.mean="green") +
  ylim(c(0,NA)) +
  scale_fill_discrete(type=cols) +
  theme(legend.position = c(.85, .2))
)
#
#
#x11()
#ggplot(subset(perf,statistic=="C"),aes(mp,data))  + geom_boxplot()
#ggplot(subset(perf,statistic=="PSBlim"),aes(mp,data))  + geom_boxplot()
#ggplot(subset(perf,statistic=="PSBlim"),aes(data,fill=mp))  + geom_density()
#risk<-subset(perf,statistic=="PSBlim" & mp=="cpue_05_targIAV")
#ggplot(subset(perf,statistic=="IACC"),aes(mp,data))  + geom_boxplot()
#
#
#subset(perf,statistic=="IACC" & mp=="MP1")$data
#iter201 <- iter( om(tuned[[1]])@stock , 201)
#Ca <-mets$C(iter201)
#mean(Ca)
#100 * yearSums(abs(Ca[, -1] - Ca[, -dim(Ca)[2]]))/yearSums(Ca)
#abs(Ca[, -1] - Ca[, -dim(Ca)[2]])
#SB<-mets$SB(iter201)
#  unitSums(quantSums(stock.n(iter201) * mat(iter201)  * stock.wt(iter201)))
#
#mets$Rec(iter201)
#plot(iter201) + geom_point()
#
# plotTOs

ggsave("report/output/tosTCMP.png",
plotTOs(perf, x="C", y=c("SBMSY", "green", "PSBlim", "IACC")) +
  ylim(c(0,NA)) +
  scale_fill_discrete(type=cols) +
  guides(fill=guide_legend(ncol=1, byrow=FALSE))
)

# kobeMPs

ggsave("report/output/kobempsTCMP.png",
kobeMPs(perf, x="SBMSY", y="FMSY", SBlim=0.40, Flim=1.4, Ftarget=NULL,
  SBtarget=NULL, probs=c(0.10, 0.50, 0.90), size=0.75, alpha=1) +
  scale_fill_discrete(type=cols) +
  guides(fill=guide_legend(ncol=1, byrow=FALSE))
)

# kobeTS

ggsave("report/output/kobetsTCMP.png",
kobeTS(kobe_perf) + theme(legend.position="bottom")
)

# PLOT time series

its <- seq(500)[c(om_metrics$SB[,'2022']) %in%
  sort(c(om_metrics$SB[,'2022']))[c(50, 250, 450)]]

#its <- sample(1:500,3)

# F

ggsave("report/output/metricsFTCMP.png",
plotOMruns(om_metrics$F, FLQuants(lapply(mp_metrics, "[[", "F")), iter=its , ylim = c(0,1.5))
)

# SSB

ggsave("report/output/metricsSBTCMP.png",
plotOMruns(om_metrics$SB, FLQuants(lapply(mp_metrics, "[[", "SB")), iter=its)
)

# CATCH

ggsave("report/output/metricsCTCMP.png",
plotOMruns(om_metrics$C, FLQuants(lapply(mp_metrics, "[[", "C")), iter=its)
)


# SUMMARY table

print(summTable(perf,statistics=c("PSBlim","IACC","green","C","SBMSY")), type="html", file="report/output/summary_tableTCMP.html")

# FINAL table

print(
resTable(year_perf[year == 2026], statistics=statistics[unique(year_perf[['statistic']])]),
type="html", file="report/output/full_table_5yTCMP.html")

print(
resTable(year_perf[year == 2031], statistics=statistics[unique(year_perf[['statistic']])]),
type="html", file="report/output/full_table_10yTCMP.html")

print(
resTable(year_perf[year == 2040], statistics=statistics[unique(year_perf[['statistic']])]),
type="html", file="report/output/full_table_20yTCMP.html")

# EXTRA

# DIFF mean ~ median Kobe(green)
# hist(perf[mp=='MP3' & statistic=='green',]$data)

# }}}








# --- OUTPUT for the effect of TAC stab in CPUE MP

load("output/outputTCMPcpuestabeffect.Rdata")

# MP colors: red, green and blue, 3 hues

nms    <- data.frame(mp=names(nms),nms)
perf<-merge(perf,nms,all.x=T)
perf$mp<-perf$nms

cols <- c( "#096D98","#E53116",  "#3195c0","#FF593E", "#59bde8","#FF8166")

# plotBPs

ggsave("report/output/bpsTCMPeffectTACstab.png",
plotBPs(perf, statistics=c("SBMSY", "green", "PSBlim", "C", "IACC"),show.mean="green") +
  ylim(c(0,NA)) +
  scale_fill_discrete(type=cols) +
  theme(legend.position = c(.85, .2))
)
#
#