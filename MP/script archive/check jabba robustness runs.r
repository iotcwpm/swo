# output.R - PREPARE mode results: metrics, performance
# swo/MP/output.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)
library(mseviz)

source("utilities.R")
#source("utilities old.R")

# LOAD statistics & OM

source("model.R")


outpath <- file.path("model", c("jabba",  "cpue","test"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)
files <- files[12]


mp <- readRDS(files)
p1<-plot(unitSums(catch(mp)))  + geom_hline(yintercept = mp@control$hcr@args$target) + ggtitle(files)

tr<-mp@tracking[[1]]
cc<-tr[c("C.om","catch.hcr"),]





# load the Jabba based tuned MPs
outpath <- file.path("model", c("jabba"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)

tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))), readRDS)

outpath2 <- "model/test"


test<- mp (om = iter(om,1:10), oem = iter(oem,1:10),  ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
control(test)$hcr@args$target   <- 50000
test2<- mp (om = iter(om,1:10), oem = iter(oem,1:10),  ctrl = test@control, args = test@args, parallel= TRUE)

tracking(test2)[c("C.om","catch.hcr"),]
tracking(test2)[c("C.om","catch.hcr"),,,,,1]

ggsave("tracking.png",
plot(tracking(test2)[c("C.om","catch.hcr"),]))


control(test2)$hcr@args$dlow   <- NA
control(test2)$hcr@args$dupp   <- NA
test3<- mp (om = iter(om,1:10), oem = iter(oem,1:10),  ctrl = test2@control, args = test2@args, parallel= TRUE)

tracking(test3)[c("C.om","catch.hcr"),]



# "model/test/jabba_hcst_05_targ_IE.Rds"

outpath <- file.path("model", c("jabba",  "cpue","test"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)
files <- files[grep("IE",files)]
files <- files[4]


mp <- readRDS(files)
p2<-plot(unitSums(catch(mp)))  + geom_hline(yintercept = mp@control$hcr@args$target) +   geom_hline(yintercept = 1.2* mp@control$hcr@args$target,linetype="dotted") + ggtitle(files)


tr<-mp@tracking[[1]]


tr$decision.hcr

targ <- mp@control$hcr@args$target


cc<-tr[c("SB.om","C.obs","catch.hcr"),]

plot(cc) + ggtitle("Model based MP (tuned for p(green)=50%)")





#  "model/test/cpue_05_targIAV_IE.Rds"

outpath <- file.path("model", c("jabba",  "cpue","test"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)
files <- files[15]


mp <- readRDS(files)
p2<-plot(unitSums(catch(mp)))  + geom_hline(yintercept = mp@control$hcr@args$target) +   geom_hline(yintercept = 1.2* mp@control$hcr@args$target,linetype="dotted") + ggtitle(files)


tr<-mp@tracking[[1]]


tr$decision.hcr

targ <- mp@control$hcr@args$target

cc<-tr[c("SB.om","C.obs","hcr"),]

plot(cc) + ggtitle("CPUE MP (tuned for p(green)=50%)")
