# test.R - DESC
# /home/mosquia/Backlog/SWO_MSE@iotc/swo/MP/test.R

# Copyright (c) WUR, 2022.
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2

source("model.R")


############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 
# robustness test to implementation error
## CPUE MP
############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 


# load the tuned MPs
outpath <- file.path("model", c("cpue"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)
files <- files[grep("targIAV",files)]


tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))), readRDS)

outpath2 <- "model/test"
#
#
#test<- mp (om = iter(om,1:10), oem = iter(oem,1:10),  ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
#test<- mp (om = om, oem ,  ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
#
#load("testcpue.RData")
#
#ssb(test)
#ssb(tuned[[1]])
#

iem <- FLiem(method=noise.iem,  args=list(noise=unitMeans(rec(om)) %=% (1.2)))


cpue_05_targIAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
saveRDS(cpue_05_targIAV_IE, file=file.path(outpath2, "cpue_05_targIAV_IE.Rds"),  compress="xz")


cpue_06_targIAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[2]]@control, args = tuned[[2]]@args, parallel= TRUE)
saveRDS(cpue_06_targIAV_IE, file=file.path(outpath2, "cpue_06_targIAV_IE.Rds"),  compress="xz")


cpue_07_targIAV_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[3]]@control, args = tuned[[3]]@args, parallel= TRUE)
saveRDS(cpue_07_targIAV_IE, file=file.path(outpath2, "cpue_07_targIAV_IE.Rds"),  compress="xz")



############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 
# robustness test to implementation error
## JABBA MP
############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ ############################ 


 # load the Jabba based tuned MPs
outpath <- file.path("model", c("jabba"))
files <- list.files(outpath, pattern="*.Rds", full.name=TRUE)

tuned <- lapply(setNames(files, nm=gsub("\\.Rds$", "", basename(files))), readRDS)

outpath2 <- "model/test"


iem <- FLiem(method=noise.iem,  args=list(noise=unitMeans(rec(om)) %=% (1.2)))


test<- mp (om = iter(om,1:10), oem = iter(oem,1:10), iem =iter(iem,1:10), ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)




jabba_hcst_05_targ  <- tuned[[1]] 
jabba_hcst_05_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[1]]@control, args = tuned[[1]]@args, parallel= TRUE)
saveRDS(jabba_hcst_05_targ_IE, file=file.path(outpath2, "jabba_hcst_05_targ_IE.2.Rds"),  compress="xz")

jabba_hcst_06_targ  <- tuned[[2]] 
jabba_hcst_06_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[2]]@control, args = tuned[[2]]@args, parallel= TRUE)
saveRDS(jabba_hcst_06_targ_IE, file=file.path(outpath2, "jabba_hcst_06_targ_IE.2.Rds"),  compress="xz")

jabba_hcst_07_targ  <- tuned[[3]] 
jabba_hcst_07_targ_IE<- mp (om = om, oem = oem, iem = iem, ctrl = tuned[[3]]@control, args = tuned[[3]]@args, parallel= TRUE)
saveRDS(jabba_hcst_07_targ_IE, file=file.path(outpath2, "jabba_hcst_07_targ_IE.2.Rds"),  compress="xz")



