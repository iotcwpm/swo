# model_tune_jabba.R - TUNE jabba.sa + hockeystick.hcr / trend.hcr MPs
# swo/MP/model_tune_jabba.R

# Copyright WMR, 2022
# Authors: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#          Thomas BRUNEL (WMR) <thomas.brunel@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


source("model.R")

library(doParallel)
registerDoParallel(50)

# PATH in anunna
outpath <- "model/jabba"

# EXTRACT args from perfect runs
load("model/perfect/args.Rdata")


# --- TUNE(target) jabba.sa + hockeystick.hcr

control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=hockeystick.hcr,
    args=list(lim=0.10, trigger=0.40, target=mean(refpts(om)$MSY) * 0.90,
      metric=relmets$SB0, output="catch", dlow=0.85, dupp=1.15))))

# P(kobe=green) = 0.5

ttarget <- perf_args$perf_hcst_05_targ$target

jabba_hcst_05_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=ttarget * c(0.75, 1.25)), prob=0.5, tol=0.02, maxit=12,
  parallel=TRUE)

saveRDS(jabba_hcst_05_targ, file=file.path(outpath, "jabba_hcst_05_targ.Rds"),
  compress="xz")

rm(jabba_hcst_05_targ)

# P(kobe=green) = 0.6

ttarget <- perf_args$perf_hcst_06_targ$target

jabba_hcst_06_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=ttarget * c(0.75, 1.25)), prob=0.6, tol=0.02, maxit=12,
  parallel=TRUE)

saveRDS(jabba_hcst_06_targ, file=file.path(outpath, "jabba_hcst_06_targ.Rds"),
  compress="xz")

rm(jabba_hcst_06_targ)

# P(kobe=green) = 0.7

ttarget <- perf_args$perf_hcst_07_targ$target

jabba_hcst_07_targ <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(target=ttarget * c(0.75, 1.25)), prob=0.7, tol=0.02, maxit=12,
  parallel=TRUE)

saveRDS(jabba_hcst_07_targ, file=file.path(outpath, "jabba_hcst_07_targ.Rds"),
  compress="xz")

rm(jabba_hcst_07_targ)


# --- TUNE(k2) jabba.sa + trend.hcr

# P(kobe=green) = 0.5

ttarget <- perf_args$perf_tren_05_k2$k2

control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=trend.hcr,
    args=list(k1=5, k2=3, gamma=1, nyears=5, metric=relmets$SB1))))

jabba_tren_05_k2 <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(k2=ttarget * c(0.75, 1.25)), prob=0.5, tol=0.02, maxit=12, parallel=TRUE)

saveRDS(jabba_tren_05_k2, file=file.path(outpath, "jabba_tren_05_k2.Rds"),
  compress="xz")

rm(jabba_tren_05_k2)

# P(kobe=green) = 0.6

control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=trend.hcr,
    args=list(k1=6, k2=2.5, gamma=1, nyears=5, metric=relmets$SB1))))

jabba_tren_06_k2 <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(k2=c(1, 4)), prob=0.6, tol=0.02, maxit=12, parallel=TRUE)

saveRDS(jabba_tren_06_k2, file=file.path(outpath, "jabba_tren_06_k2.Rds"),
  compress="xz")

rm(jabba_tren_06_k2)

# P(kobe=green) = 0.7

control <- mpCtrl(list(
  est = mseCtrl(method=jabba.sa),
  hcr = mseCtrl(method=trend.hcr,
    args=list(k1=6.5, k2=2, gamma=1, nyears=5, metric=relmets$SB1))))

jabba_tren_07_k2 <- tunebisect(om, oem=oem, control=control, args=mseargs,  
  metrics=mets[c("SB", "F")], statistic=statistics["green"], years=tperiod,
  tune=list(k2=c(1, 3)), prob=0.7, tol=0.02, maxit=12, parallel=TRUE)

saveRDS(jabba_tren_07_k2, file=file.path(outpath, "jabba_tren_07_k2.Rds"),
  compress="xz")

rm(jabba_tren_07_k2)


# FINAL list

files <- list.files('model', pattern="*.Rds", full.name=TRUE)
tune <- lapply(setNames(files, gsub("\\.Rds$", "", basename(files))), readRDS)

save(tune, file="model/tune_jabba.Rdata", compress="xz")
