
# hr.hcr {{{

hr.hcr <- function(stk, ind, metric="mean", target, trigger,
  lambda=1, dlow=NA, dupp=NA, args, tracking) {

  # args
  ay <- args$ay
  frq <- args$frq
  data_lag <- args$data_lag
  man_lag <- args$management_lag
  dys <- ac(ay - data_lag)
  mys <- ac(seq(ay + man_lag, length.out=frq))

  # SELECT metric
  met <- ind[[metric]]

  # CALCULATE biomass safeguard, min(1, I_{ay-dlag} / Itrigger)
  bsg <- qmin(met / trigger, 1)

  # SET new TAC
  tac <- met * target * bsg * lambda

  # TRACK initial tac
  track(tracking, paste0("tac.hcr"),    seq(ay + man_lag, ay + frq)) <- c(tac)
  track(tracking, paste0("hr.hcr.bsg"), seq(ay + man_lag, ay + frq)) <- c(bsg)
  
  # GET previous TAC | catch
  pre <- tracking[[1]]["hcr", dys]
  nas <- c(is.na(pre))
  if(any(nas)) {
    iter(pre, nas) <- iter(catch(stk)[, ac(ay - frq)], nas)
  }

  # TODO: PA buffer

  # APPLY limits
  if(!is.na(dupp))
    tac[tac > pre * dupp] <- pre[tac > pre * dupp] * dupp
  if(!is.na(dlow))
    tac[tac < pre * dlow] <- pre[tac < pre * dlow] * dlow

  # CONSTRUCT fwd control
  ctrl <- as(FLQuants(catch=expand(tac, year=mys)), "fwdControl")

  list(ctrl=ctrl, tracking=tracking)
}

# }}}

# hr.target {{{

hr.target <- function(len, catch, index, lc, linf) {

  # COMPUTE reference length
  reflen <- 0.75 * lc + 0.25 * linf

  # IDENTIFY years where mean length >= reflen
  iyr <- dimnames(len)$year[len >= reflen]

  # CALCULATE harvest rate for selected years only
  hr <- catch[, iyr] / index[, iyr]

  return(yearMeans(hr))
}
# }}}




 cpue.ind  <- function(stk, idx, nyears=5, ayears=3, index=1, args, tracking) {

  # ARGS
  ay <- args$ay
  dlag <- args$data_lag
  dyrs <- ac(seq(ay - dlag - (nyears - 1) , length=nyears))

  # SUBSET last nyears from ay - mlag
  met <- index(idx[[index]])[1, dyrs]

  # SLOPE by iter
  dat <- data.table(as.data.frame(met))
  slope <- dat[, .(data=coef(lm(log(data) ~ year))[2]), by=iter]

  # WEIGHTED average index of last ayears
  ywts <- c(0.50 * seq(1, ayears - 1) / sum(seq(1, ayears - 1)), 0.50)
  wmean <- expand(yearSums(tail(met, ayears) * ywts), year=ay - dlag)

  # AVERAGE index
  mean <- expand(yearMeans(tail(met, ayears)), year=ay - dlag)

  # OUTPUT
  slope <- FLQuant(slope$data, dimnames=dimnames(mean), units="")
  ind <- FLQuants(wmean=wmean, slope=slope, mean=mean)

  # TRACK
  track(tracking, "mean.ind", ac(ay)) <- mean
  track(tracking, "wmean.ind", ac(ay)) <- wmean
  track(tracking, "slope.ind", ac(ay)) <- slope

  return(list(stk=stk, ind=ind, tracking=tracking))

}
