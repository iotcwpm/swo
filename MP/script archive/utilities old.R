# utilities.R - DESC
# /utilities.R

# Copyright Iago MOSQUEIRA (WMR), 2021
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


# }}}

# metrics {{{

# OVERALL

mets <- list(Rec=function(x) unitSums(rec(x)), SB=function(x) unitSums(ssb(x)),
  C=function(x) unitSums(catch(x)), F=function(x) unitMeans(fbar(x)))

# RELATIVE metrics

relmets <- list(
  SBMSY=function(x) unitSums(ssb(x)) %/% refpts(x)$SBMSY,
  SB0=function(x) unitSums(ssb(x)) %/% refpts(x)$SB0,
  B0=function(x) unitSums(stock(x)) %/% refpts(x)$B0,
  SB1=function(x) unitSums(ssb(x)) %/% unitSums(ssb(x)[,1]),
  BK=function(x) unitSums(stock(x)) %/% refpts(x)$K,
  FMSY=function(x) unitMeans(fbar(x)) %/% refpts(x)$FMSY) 

# }}}

# ---

