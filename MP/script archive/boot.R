# boot.R - INSTALL packages and copy OM file.
# swo/MP/boot.R

# Copyright Iago MOSQUEIRA (WMR), 2021
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(remotes)

# INSTALL latest versions from github

install_github(c("flr/FLCore", "flr/ggplotFL", "flr/FLFishery",
  "flr/FLasher", "flr/mse"), upgrade="never")

# INSTALL JABBA

install_github(c("jabbamodel/JABBA"), upgrade="never")

# COPY ../OM/output/om.Rdata  to data/

file.copy('../OM/output/om.Rdata', 'data/', overwrite=TRUE)

# GET utilities.R from alb repository

download.file("https://git.wur.nl/iotcwpm/alb/-/raw/main/MP/utilities.R", 
    destfile = "utilities.R", method = "libcurl")
