



cpue<- apply(window(index(observations(oem)$idx[[1]]),end=2022),1:5,median)
target <- 0.35 # target obtained by tuning for 50% p green


res<-data.frame(year=1997:2022,sl=NA,diff=NA,diff2=NA,diff3=NA,ind="cpue")

for (y in 1997:2022)
{

dat <- data.table(as.data.frame(window(cpue,start=c(y-4),end=y)))

res$sl[res$year ==y] <-coef(lm(log(data + 1e-22) ~ year, data=dat))[2]
res$diff[res$year ==y] <-    mean(dat$data,na.rm=T) - target
res$diff2[res$year ==y] <-  (  mean(dat$data,na.rm=T) - target)/target
res$diff3[res$year ==y] <-    log(mean(dat$data,na.rm=T)) - log(target)
}


pl <- tidyr::gather(res[,c(1:5)],key=ind,value=value,2:5)
ggplot(pl,aes(ind,abs(value))) + geom_boxplot()

ggplot(pl , aes(year,value,group=ind)) + geom_line(aes(colour=ind))




cpue<- 10 * apply(window(index(observations(oem)$idx[[1]]),end=2022),1:5,median)
target <- 10 * target



res2<-data.frame(year=1997:2022,sl=NA,diff=NA,diff2=NA,diff3=NA,ind="3 time cpue")

for (y in 1997:2022)
{

dat <- data.table(as.data.frame(window(cpue,start=c(y-4),end=y)))

res2$sl[res$year ==y] <-coef(lm(log(data + 1e-22) ~ year, data=dat))[2]
res2$diff[res$year ==y] <-    mean(dat$data,na.rm=T) - target
res2$diff2[res$year ==y] <-  (  mean(dat$data,na.rm=T) - target)/target
res2$diff3[res$year ==y] <-    log(mean(dat$data,na.rm=T)) - log(target)
}

pl2 <- tidyr::gather(res2[,c(1:5)],key=ind,value=value,2:5)
ggplot(pl2,aes(ind,abs(value))) + geom_boxplot()

ggplot(pl2 , aes(year,value,group=ind)) + geom_line(aes(colour=ind))


pl$idx  <- "org"
pl2$idx <- "x10"

ggplot(rbind(pl,pl2),aes(ind,abs(value))) + geom_boxplot() + facet_grid(idx~.)



res<-   rbind(res,res2)


k12=0.1
k34=0.1

res$TACmult <- 1 + k12 *res$sl + k34 *res$diff
res$TACmult2 <- 1 + k12 *res$sl + k34 *res$diff2


library(ggplot2)
p1<-plot(cpue)
p2<-ggplot(res, aes(year,TACmult))  + geom_line(aes(col=ind)) + geom_point(aes(shape=ind,col=ind)) + geom_hline(yintercept=1) +
  scale_shape_manual(values=c(11, 20))
p3<-ggplot(res, aes(year,TACmult2)) + geom_line(aes(col=ind)) + geom_point(aes(shape=ind,col=ind)) + geom_hline(yintercept=1) +
  scale_shape_manual(values=c(11, 20))

library(patchwork)
 p1 + p2 + p3


