
library(mse)

load("./output/om.Rdata")

rec <- window(unitSums(rec(stock(om))),end = 2022)
ssb <- window(unitSums(ssb(stock(om))),end = 2022)
fbar   <- window(unitMeans(fbar(stock(om))),end = 2022)
catch <-  window(unitSums(catch(stock(om))),end = 2022)

library(ggplot2)
library(patchwork)
plot(rec) +ylab("Recruitment (thousands)") +
plot(ssb)+ylab("SB (tonnes)") +
plot(fbar)+ ylab("Fbar") +
plot(catch) +ylab("Catch (tonnes)") +
  plot_layout(nrow = 4)
  
  
sbmsy <- om@refpts$SBMSY
sbsbmsy <- ssb / sbmsy

sbmsy <- om@refpts$SBMSY
sbsbmsy <- ssb / sbmsy











x<-window(om ,end =2021)

  dat <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(x))  / refpts(x)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(x)) / refpts(x)$FMSY)

  p1 <- plot(dat) + geom_hline(yintercept=1, linetype=2) +
    facet_grid(qname~., labeller=label_parsed, scales="free_y")

ymax <- max(p1$data$data)

ymax <- 6

 p1 <- p1 + ylim(c(0, (ymax)))

  fin <- data.table(p1$data)[date == max(date), ]
  fin[,y_min := 0]
  fin[,y_max:= 6, by = qname]


  p2 <- ggplot(fin, aes(y=data)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(color=flpalette_colours(1), fill=flpalette_colours(1),
      alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 6))+
    theme(legend.position="none") +
    ylim(c(0, ymax)) +
    xlab("") + ylab("") +
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)

  # ADD final year label in x-axis
  p2 <- p2 + scale_x_continuous(breaks=0.50, labels=unique(fin$year))

  # ASSEMBLE plot with c(4,1) horizontal proportions
  p1 + p2 + plot_layout(widths = c(4, 1))
}


#####
# compare with the new 2023 SWO assessment

load("./data/2023_sa_swo.rda")

med<-unitSums(apply(ssb(newsaswo),1:5,median))
qlow<-unitSums(apply(ssb(newsaswo),1:5,function(x) quantile(x,0.05)))
qhigh<-unitSums(apply(ssb(newsaswo),1:5,function(x) quantile(x,0.95)))

a<-plot(unitSums(ssb( window(stock(om),end=2021)))) +   
      geom_line(data=as.data.frame(med),aes(year,data),col="blue",size=2) +
      geom_line(data=as.data.frame(qlow),aes(year,data),col="blue") +
      geom_line(data=as.data.frame(qhigh),aes(year,data),col="blue")     + ggtitle("SB") 
      

med<-unitMeans(apply(fbar(newsaswo),1:5,median))
qlow<-unitMeans(apply(fbar(newsaswo),1:5,function(x) quantile(x,0.05)))
qhigh<-unitMeans(apply(fbar(newsaswo),1:5,function(x) quantile(x,0.95))) 

b<-plot(unitMeans(fbar( window(stock(om),end=2021)))) +   
      geom_line(data=as.data.frame(med),aes(year,data),col="blue",size=2) +
      geom_line(data=as.data.frame(qlow),aes(year,data),col="blue") +
      geom_line(data=as.data.frame(qhigh),aes(year,data),col="blue")   + ggtitle("Fbar")   
      
      
 
 

med<-unitSums(apply(rec(newsaswo),1:5,median))
qlow<-unitSums(apply(rec(newsaswo),1:5,function(x) quantile(x,0.05)))
qhigh<-unitSums(apply(rec(newsaswo),1:5,function(x) quantile(x,0.95)))

c<-plot(unitSums(rec( window(stock(om),end=2021)))) +   
      geom_line(data=as.data.frame(med),aes(year,data),col="blue",size=2) +
      geom_line(data=as.data.frame(qlow),aes(year,data),col="blue") +
      geom_line(data=as.data.frame(qhigh),aes(year,data),col="blue")     + ggtitle("Recruitment") 
       


library(patchwork)

ggsave("report/OMcomparisonWPB2023assessment_trends.png",      
print( a +b +c + plot_layout(ncol=1)) )

   
    
    
load("./data/2023_rps.rda")    
    
rps1<-as.data.frame(rps)
rps1$basis<- "WPB2023"

rps2<-as.data.frame(om@refpts)
rps2$basis <- "OM"

rps<-rbind(rps1,rps2)

ggplot(subset(rps, params %in% c("SBMSY","FMSY")),  aes(params , data,fill=basis)) +geom_boxplot() + facet_grid(params~.,scales="free_y")
ggplot(rps,  aes(params , data,fill=basis)) +geom_boxplot() + facet_grid(params~.,scales="free_y")

ggsave("report/OMcomparisonWPB2023assessment_refpts.png",      
ggplot(rps,  aes( data,fill=basis)) +geom_density(alpha=0.5) + facet_wrap(params~.,scales="free"))


# stock status plots 

assess <- FLom(stock  = newsaswo , refpts = rps)


## MAKE a plot that combines the OM and the assessmentq()
x<-window(om ,end =2021)
y<-window(assess ,end =2021)

x<-om
y<-assess



 dat <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(x))  / refpts(x)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(x)) / refpts(x)$FMSY)
  
 dat2 <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(y))  / refpts(y)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(y)) / refpts(y)$FMSY) 
  
dat<-as.data.frame(dat,drop=T) 
dat2<-as.data.frame(dat2,drop=T)   

dat$basis <- "OM"
dat2$basis <- "WPB2023"

dat <- rbind(dat,dat2)
dat <- subset(dat, year < 2022)

d1<-aggregate(data~ year + qname + basis , dat,median)
names(d1)[4] <- "median"
d2<-aggregate(data~ year + qname + basis , dat,function(x) quantile(x,0.05))
names(d2)[4] <- "Q05"
d3<-aggregate(data~ year + qname + basis , dat,function(x) quantile(x,0.90))
names(d3)[4] <- "Q95"

dd<-merge(d1,merge(d2,d3,all=T),all=T)


  p1 <- ggplot(subset(dd,qname=="SB/SB[MSY]"),aes(year,median,col=basis)) + geom_ribbon(aes(ymin = Q05,ymax=Q95,fill=basis),alpha=0.3) + 
    geom_line() + geom_hline(yintercept=1, linetype=2) + ylim(c(0,11))  + ylab("SB/SB[MSY]")


  p2 <- ggplot(subset(dd,qname=="F/F[MSY]"),aes(year,median,col=basis)) + geom_ribbon(aes(ymin = Q05,ymax=Q95,fill=basis),alpha=0.3) + 
    geom_line() + geom_hline(yintercept=1, linetype=2)  + ylim(c(0,3))  + ylab("F/F[MSY]")


  fin <- data.table(dat)[year == 2021, ]
  fin[,y_min := 0]
  fin[,y_max:= 6, by = qname]


  p3 <- ggplot(subset(fin,qname=="SB/SB[MSY]"), aes(y=data,fill = basis)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 6))+
    theme(legend.position="none") +
    xlim(c(0,2))   +    ylim(c(0,11)) +
    xlab("") + ylab("SB(2021)/SB[MSY]")+
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)
      
      
     
  p4 <- ggplot(subset(fin,qname=="F/F[MSY]"), aes(y=data,fill = basis)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 25))+
    theme(legend.position="none") +
    xlim(c(0,25))   +    ylim(c(0,3)) +
    xlab("") +  ylab("F(2021)/F[MSY]")+
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)
      

  # ASSEMBLE plot with c(4,1) horizontal proportions
 
ggsave("report/OMcomparisonWPB2023assessment_status2021.png",       
  p1 + p3 + p2 + p4 + plot_layout(widths = c(4, 4))     )

