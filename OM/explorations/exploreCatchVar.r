# output.R - DESC
# /output.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)
library(mseviz)
library(rpart)
library(rpart.plot)
source("../MP/utilities.R")
data(statistics)
class(statistics)
statistics[["TAC_IAV"]] <-  statistics[[1]]
statistics[["TAC_IAV"]]$name    <- "mean_abs_rel_change"
statistics[["TAC_IAV"]]$desc    <- "mean interannual TAC variation"
statistics[["TAC_IAV"]][[1]]    <- ~ 3*yearMeans(abs(C[,-1] -C[,-dim(C)[2]] ) / C[,-dim(C)[2]])

load(file="../OM/output/om.Rdata")
HS0.6<-readRDS("../MP/model/perfect/perf_hcst_06_targ.Rds")
U0.6 <-readRDS("../MP/model/cpue/cpue_06_targ.Rds")
tr0.6 <-readRDS("../MP/model/perfect/perf_tren_06_k2.Rds")

expl<-list(HS0.6,U0.6,tr0.6)
names(expl) <- c("HS0.6","U0.6","tr0.6")



stats <- statistics[c( "green","C","TAC_IAV")]
perf <- performance(expl, metrics=mets, statistics=stats, years=list(2034:2039))

plotBPs(perf) 

head(perf)
aggregate(data~ mp + statistic , perf,median)
aggregate(data~ mp + statistic, perf,mean)      # it is tuned so that mean across iter is 0.6

# visualise catch trajectories

ca<-lapply(expl , function(x)  as.data.frame(unitSums(catch(om(x)))))
ca<-lapply(as.list(names(ca)) , function(x) {
                          sb<- ca[[x]]
                          sb$mp <-x
                          return(sb)
                          })

 cc<-do.call(rbind,ca)

q50cc <- aggregate(data~year+mp,cc,function(x) quantile(x,0.5))
q05cc <- aggregate(data~year+mp,cc,function(x) quantile(x,0.05))
q95cc <- aggregate(data~year+mp,cc,function(x) quantile(x,0.95))
names(q50cc)[3]<-"med" 
names(q05cc)[3]<-"p05"
names(q95cc)[3]<-"p95"

qcc <-  Reduce(function(x, y) merge(x, y, all=TRUE),list(q50cc,q05cc,q95cc))


its <- 1:5
ggplot(data=qcc , aes(year , med,fill=mp)) + geom_ribbon(aes(ymin=p05,ymax=p95),alpha = 0.2) + geom_line(aes(colour=mp)) +
       geom_line(data=subset(cc , iter %in% its) , aes(year,data,group=iter,colour=iter)) +
       facet_wrap(~mp,ncol=1)  
       
       

Com<-as.data.frame(HS0.6@tracking$A["C.om",], drop = T  )
Bom<-as.data.frame(HS0.6@tracking$A["B.om",], drop = T  )  
names(Com)[3] <- "C.om"
names(Bom)[3] <- "B.om"

hcr <- merge(Com,Bom)


adyr <- seq(2022,2040,by=3)

ggplot(subset(hcr, iter %in% sample(1:500,6,replace=F) & year %in% adyr) , aes(B.om,C.om,group=iter)) + geom_point(aes(colour=iter))+ geom_path() + facet_wrap(~iter) 

       
       
       