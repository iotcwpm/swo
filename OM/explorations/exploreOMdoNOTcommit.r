# output.R - DESC
# /output.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2

  
  library(mse)
  library(mseviz)
  library(rpart)
  library(rpart.plot)
  source("../../MP/utilities.R")
  


load("../model/partial.Rdata")
pdf("./plots/OMsubgridsJan2023.pdf")


# --- RESAMPLE with weighting

# GENERATE weights

results[, weight:=pvalue]  
dats <- as.data.frame(results)

library(ggplot2)
ggplot(dats,aes(SSB_status)) + geom_histogram() + xlim(0,3)
ggplot(dats,aes(SSB_status)) + geom_boxplot() + xlim(0,3)


dats <- dats[,c(2:8,30)]
#for (cc in 1:7) dats[,cc] <- as.factor(paste(substring(names(dats)[cc],1,2),dats[,cc],sep=""))
for (cc in 1:7) dats[,cc] <- as.factor(dats[,cc])


m1 <- rpart(SSB_status ~ (M) + (steepness)  + sigmaR  + ess +  growmat + cpue , data = dats , method = "anova" )



rpart.plot(m1)


# create groups of iters based on that
grps<-as.data.frame(results)
grps <- grps[,c(2:8,30)]
for (cc in 1:7) grps[,cc] <- as.character(grps[,cc])
grps$grp <- NA

grps$grp [grps$steepness == 0.9 &  grps$ess!= 2 ] <-  9
grps$grp [grps$steepness == 0.9 &  grps$ess== 2  & grps$M !=0.2] <-  8
grps$grp [grps$steepness == 0.9 &  grps$ess== 2  & grps$M ==0.2] <-  7

grps$grp [grps$steepness != 0.9 &  grps$ess!= 2 & grps$M != 0.2 & grps$steepness != 0.6] <-  6
grps$grp [grps$steepness != 0.9 &  grps$ess!= 2 & grps$M != 0.2 & grps$steepness == 0.6] <-  5
grps$grp [grps$steepness != 0.9 &  grps$ess!= 2 & grps$M == 0.2 ] <-  4

grps$grp [grps$steepness != 0.9 &  grps$ess== 2 & grps$sigmaR == 0.2 ] <-  3
grps$grp [grps$steepness != 0.9 &  grps$ess== 2 & grps$sigmaR != 0.2  & grps$M==0.3] <-  2
grps$grp [grps$steepness != 0.9 &  grps$ess== 2 & grps$sigmaR != 0.2  & grps$M!=0.3] <-  1








unique(grps[,c(1:4,9)])

ggplot(grps,aes(as.character(grp),SSB_status)) + geom_boxplot()

aggregate(grps$SSB_status~as.character(grp) ,data= grps , function(x) round(mean(x),1))


  
set.seed(47)
its <- 500
samps <- sample.int(dim(results)[1], size=its, prob=results[,weight],
  replace=TRUE)

groups <- grps$grp[samps]
res <-results[samps,]



load(file="../output/om.Rdata")
HS0.6<-readRDS("../MP/model/perfect/perf_hcst_06_targ.Rds")

SSBor<-unitSums(ssb(om))
SSB_HS0.6<-unitSums(ssb(om(HS0.6)))
SSBor[,ac(2022:2039)] <- SSB_HS0.6[,ac(2022:2039)]
SSB_HS0.6<-SSBor
#

plot(iter(unitSums(SSB_HS0.6),grep(1,groups)),iter(unitSums(SSB_HS0.6),grep(2,groups)),iter(unitSums(SSB_HS0.6),grep(3,groups)),iter(unitSums(SSB_HS0.6),grep(4,groups)),iter(unitSums(SSB_HS0.6),grep(5,groups))) +
  geom_vline(xintercept = 2022)  + xlim(2000,2040)
#
plot(iter(unitSums(SSB_HS0.6),grep(1,groups)),iter(unitSums(SSB_HS0.6),grep(2,groups)),iter(unitSums(SSB_HS0.6),grep(3,groups)),iter(unitSums(SSB_HS0.6),grep(4,groups)),iter(unitSums(SSB_HS0.6),grep(5,groups))) +
  geom_vline(xintercept = 2022)  + xlim(2000,2022) + ylim(0,3.25e5)  + ylab("SSB")

BBmsy <- unitSums(SSB_HS0.6)/ refpts(om)$SBMSY


plot(iter(BBmsy,grep(1,groups)),iter(BBmsy,grep(2,groups)),iter(BBmsy,grep(3,groups)),iter(BBmsy,grep(4,groups)),iter(BBmsy,grep(5,groups))) +
  geom_vline(xintercept = 2022)  + xlim(2000,2022)  + ylab("SSB/SSBmsy")





#pgreen
SB <- unitSums(ssb(om(HS0.6)))
SBmsy <-  refpts(om)$SBMSY
FBAR <- unitMeans(fbar(om(HS0.6)))
Fmsy <-   refpts(om)$FMSY

pGreen <-((SB / SBmsy >1)  *  (FBAR/Fmsy<1))
pgreen <- apply(pGreen[,ac(2034:2039)],c(1,3:6),function(x) sum(x) / length(x)) 
pgreen <- as.data.frame(pgreen,drop=T)


ggplot(pgreen, aes(data)) + geom_boxplot()

mean(pgreen$data,na.rm=T)






ssbdt <- as.data.frame(  SSB_HS0.6,drop=T)
ssbdt <- merge(ssbdt , data.frame(iter=1:500,group=groups),all.x=T)



bmsy <-as.data.frame(refpts["SBMSY"],drop=T)
names(bmsy)[2]<-"Bmsy"
bmsy <-bmsy[samps,]
bmsy$iter <-1:500           # check : plot(bmsy$Bmsy,results$SSB_MSY)


ssbdt<-merge(ssbdt , bmsy,all.x=T)
ssbdt$BBmsy <- ssbdt$data /ssbdt$Bmsy

ggplot(subset(ssbdt ,year == 2020) , aes(as.character(group),BBmsy)) + geom_boxplot()  + ggtitle("BBmsy per group 2020")
ggplot(subset(ssbdt ,year %in% 2034:2039) , aes(as.character(group),BBmsy)) + geom_boxplot()  + ggtitle("BBmsy per group 2034-39")


setMethod("iter", signature(obj="FLmse"),
  function(obj, iter) {

    # OM
    Om <- iter(om(obj), iter)
    # OEM
    OeM <- iter(oem(obj), iter)
    # tracking
    trc <- iter(slot(obj, "tracking"), iter)
    obj<-FLmse(om=Om,oem=OeM,tracking = trc)
    return(obj)
  }
)

MP<- HS0.6
oem(MP) <- oem


MPs <- lapply(1:5 , function(its)    iter(MP,grep(its,groups)))
names(MPs)<- paste0("OMgroup", 1:5)

data(statistics)
stats <- statistics[c("SB0",  "SBMSY", "FMSY", "green", "red",  "C")]
perf1 <- performance(MPs, metrics=mets, statistics=stats, years=list(2022))

plotBPs(perf, statistics=c("SBMSY",  "FMSY",   "green",   "C")) +   
   ggtitle("perfect-hcst-0.6targ 2022")

perf2 <- performance(MPs, metrics=mets, statistics=stats, years=list(2034:2039))

plotBPs(perf, statistics=c("SBMSY",  "FMSY",   "green",   "C")) +   
   ggtitle("perfect-hcst-0.6targ 2034:2039")



kobe_perf <- performance(MPs, metrics=mets, statistics=kobestatistics, years=2034:2039)

kobeTS(kobe_perf) + theme(legend.position="bottom")   + ggtitle("perfect-hcst-0.6targ")


perfMP <- performance(MP, metrics=mets, statistics=statistics["green"], years=list(2034:2039))
 plot(table(perfMP$data))

ggplot(perfMP ,aes(data)) + geom_histogram() + xlab("p(Green) 2034:2039") + ylab("number of iterations") + geom_vline(xintercept = mean(perfMP$data))
mean(perfMP$data)


dev.off()


HS0.6<-readRDS("../MP/model/perfect/old-perf_hcst_06_targ.Rds")
MP<- HS0.6
perfMP <- performance(MP, metrics=mets, statistics=statistics["green"], years=list(2034:2039))
ggplot(perfMP ,aes(data)) + geom_histogram() + xlab("p(Green) 2034:2039") + ylab("number of iterations") + geom_vline(xintercept = mean(perfMP$data))
mean(perfMP$data)

perfMP <- performance(MP, metrics=mets, statistics=statistics["green"], years=2022)




