# model_partial_load.R - DESC
# /model_partial_load.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(ss3om)

library(doParallel)
registerDoParallel(25)

# --- LOAD OM grid

load("model/partial/grid.Rdata")

dirs <- list.dirs("model/jitter", recursive=FALSE)

results<-list()

for (run in dirs) 
{
   res<-readOMSss3(run)$results
   res$run <- run
res   
}


results <- lapply(as.list(dirs) , function(run)
                      {
                      res<-readOMSss3(run)$results
                      res$run <- run
                      return(res)}
                      )

results2 <- do.call(rbind.data.frame, results)
results2$run <- sort(rep(c(1,63,113,119,171),10))
results2$jitt <- (rep(c(1:10),5))



save(results2, file="model/jitter/jitter.Rdata", compress="xz")

load( file="model/jitter/jitter.Rdata")
res<-results2[,c("run","jitt","LIKELIHOOD","Convergence_Level","SSB_Virgin","Recr_Virgin","F_status","SSB_status")]

library(ggplot2)
ggplot(res, aes(factor(run),SSB_status)) + geom_boxplot()
ggplot(res, aes(factor(run),F_status)) + geom_boxplot()

ggplot(res, aes(F_status,SSB_status,group=factor(run))) + geom_point(aes(col=factor(run)))
ggplot(res, aes(jitt , LIKELIHOOD)) +geom_point() + facet_wrap(~factor(run),scales= "free") 




load("../OM/model/partial.Rdata")


res$valid <- res$run %in% results$iter

results<-subset(results ,  iter %in%   res$run)
results$run <- results$iter
results$main_run_loglik<-results$LIKELIHOOD

res<-merge(res,results[,c("run","main_run_loglik")] )
res[,c(1,2,9)]

ggplot(res, aes(factor(run) , LIKELIHOOD,fill=valid)) + geom_boxplot()  + facet_wrap(~factor(run),scales= "free")   + 
                      geom_hline(aes(yintercept=main_run_loglik))







 load("../OM/model/partial.Rdata")
 
 results$Bmsyratio <- results$SSB_MSY/results$SSB_Virgin
 
ggplot(results , aes(Bmsyratio)) + geom_density()   + xlab("SBMSY/SSBVirgin")
 