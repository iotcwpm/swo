#C starter file written by R function SS_writestarter
#C rerun model to get more complete formatting in starter.ss_new
#C should work with SS version: 3.30
#C file write time: 2021-04-15 15:17:15
#
swo.dat #_datfile
swo.ctl #_ctlfile
0 #_init_values_src
1 #_run_display_detail
1 #_detailed_age_structure
0 #_checkup
0 #_parmtrace
1 #_cumreport
0 #_prior_like
1 #_soft_bounds
1 #_N_bootstraps
10 #_last_estimation_phase
10 #_MCMCburn
2 #_MCMCthin
0 #_jitter_fraction
1948 #_minyr_sdreport
2028 #_maxyr_sdreport
0 #_N_STD_yrs
1e-04 #_converge_criterion
-3 #_retro_yr
1 #_min_age_summary_bio
2 #_depl_basis
1 #_depl_denom_frac
2 #_SPR_basis
5 #_F_report_units
2 8 #_F_age_range
2 #_F_report_basis
0 #_MCMC_output_detail
0 #_ALK_tolerance
#
-1 #_seed
3.3 #_final
