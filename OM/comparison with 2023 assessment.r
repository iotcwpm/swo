
library(mse)

load("./output/om.Rdata")
load("./data/2023_sa_swo.rda")



range(newsaswo)["minfbar"] <-2
range(newsaswo)["maxfbar"] <-8
    
load("./data/2023_rps.rda")    
    
rps1<-as.data.frame(rps)
rps1$basis<- "WPB2023"

rps2<-as.data.frame(om@refpts)
rps2$basis <- "OM"

rpss<-rbind(rps1,rps2)

ggplot(subset(rpss, params %in% c("SBMSY","FMSY")),  aes(params , data,fill=basis)) +geom_boxplot() + facet_grid(params~.,scales="free_y")
ggplot(rpss,  aes(params , data,fill=basis)) +geom_boxplot() + facet_grid(params~.,scales="free_y")

ggsave("report/OMcomparisonWPB2023assessment_refpts.png",      
ggplot(rpss,  aes( data,fill=basis)) +geom_density(alpha=0.5) + facet_wrap(params~.,scales="free"))


# stock status plots 

assess <- FLom(stock  = newsaswo , refpts = rps)


## MAKE a plot that combines the OM and the assessmentq()
x<-window(om ,end =2021)
y<-window(assess ,end =2021)

x<-om
y<-assess



 dat <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(x))  / refpts(x)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(x)) / refpts(x)$FMSY)
  
 dat2 <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(y))  / refpts(y)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(y)) / refpts(y)$FMSY) 
  
dat<-as.data.frame(dat,drop=T) 
dat2<-as.data.frame(dat2,drop=T)   

dat$basis <- "OM"
dat2$basis <- "WPB2023"

dat <- rbind(dat,dat2)
dat <- subset(dat, year < 2022)

d1<-aggregate(data~ year + qname + basis , dat,median)
names(d1)[4] <- "median"
d2<-aggregate(data~ year + qname + basis , dat,function(x) quantile(x,0.05))
names(d2)[4] <- "Q05"
d3<-aggregate(data~ year + qname + basis , dat,function(x) quantile(x,0.90))
names(d3)[4] <- "Q95"

dd<-merge(d1,merge(d2,d3,all=T),all=T)


  p1 <- ggplot(subset(dd,qname=="SB/SB[MSY]"),aes(year,median,col=basis)) + geom_ribbon(aes(ymin = Q05,ymax=Q95,fill=basis),alpha=0.3) + 
    geom_line() + geom_hline(yintercept=1, linetype=2) + ylim(c(0,11))  + ylab("SB/SB[MSY]")


  p2 <- ggplot(subset(dd,qname=="F/F[MSY]"),aes(year,median,col=basis)) + geom_ribbon(aes(ymin = Q05,ymax=Q95,fill=basis),alpha=0.3) + 
    geom_line() + geom_hline(yintercept=1, linetype=2)  + ylim(c(0,3))  + ylab("F/F[MSY]")


  fin <- data.table(dat)[year == 2021, ]
  fin[,y_min := 0]
  fin[,y_max:= 6, by = qname]


  p3 <- ggplot(subset(fin,qname=="SB/SB[MSY]"), aes(y=data,fill = basis)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 6))+
    theme(legend.position="none") +
    xlim(c(0,2))   +    ylim(c(0,11)) +
    xlab("") + ylab("SB(2021)/SB[MSY]")+
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)
      
      
     
  p4 <- ggplot(subset(fin,qname=="F/F[MSY]"), aes(y=data,fill = basis)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 25))+
    theme(legend.position="none") +
    xlim(c(0,25))   +    ylim(c(0,3)) +
    xlab("") +  ylab("F(2021)/F[MSY]")+
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)
      

  # ASSEMBLE plot with c(4,1) horizontal proportions
library(patchwork) 
ggsave("report/OMcomparisonWPB2023assessment_status2021.png",       
  p1 + p3 + p2 + p4 + plot_layout(widths = c(4, 4))     )







# look at recruitment deviates

omRdev <- residuals(om@sr)[,,1,,]

dt <- read.csv("./data/2023SA/rec.csv")
names(dt)[1] <- "year"

library(corrplot)
png("report/SA2023_RdeviancesCor.png")       
corrplot(cor(dt[,-1]))
dev.off()


Rdev<-window(iter(omRdev,seq(1,c(dim(dt)[2]-1))) , start=min(dt$year), end=max(dt$year))
for (it in  seq(1,c(dim(dt)[2]-1))) Rdev[,,,,,it] <- dt[,it+1]


ggsave("report/OMcomparisonWPB2023assessment_Rdeviances.png",       
plot(window(log(omRdev),start= 2010,end=2020)) +ylim(-0.8,1) + geom_hline(yintercept = 0)  + geom_vline(xintercept = 2017)+  ggtitle("SR devs OM") +
plot(window(Rdev,start=2010,end=2020))         +ylim(-0.8,1) + geom_hline(yintercept = 0) + geom_vline(xintercept = 2017)+  ggtitle("SR devs 2023SA")
                                                       )





# look at fisheries selectivity


tp <- 2018:2021

#selOM      <-  as.data.frame(harvest(om)[,ac(tp)]      %/%  fbar(om)[,ac(tp)],drop=T)
#selAssess  <-  as.data.frame(harvest(assess)[,ac(tp)]  %/%  fbar(assess)[,ac(tp)], drop=T)

selOM      <-  as.data.frame(harvest(om)[,ac(tp)]      ,drop=T)
selAssess  <-  as.data.frame(harvest(assess)[,ac(tp)]  , drop=T)



selOM$basis <- "OM"
selAssess$basis <- "SA"
sel <- rbind(selOM,selAssess)

sel <- aggregate(data~ age+basis+unit+iter,sel,mean)

 
p50 <- aggregate(data~ age + basis + unit, sel,median) 
p05 <- aggregate(data~ age + basis + unit, sel,function(x) quantile(x,0.05)) 
p95 <- aggregate(data~ age + basis + unit, sel,function(x) quantile(x,0.95)) 
 
names(p50)[4] <- "median" 
names(p05)[4] <- "low"
names(p95)[4] <- "high"


sels<- merge(merge(p50,p05),p95)

ggplot(sels,aes(age,median,group=basis))  + 
geom_ribbon(aes(x=age,ymin=low,ymax=high,fill=basis),alpha=0.3) + 
geom_line(aes(age,median,group=basis,col=basis)) +facet_grid(unit~.) +
ylab("fishing mortality")






 