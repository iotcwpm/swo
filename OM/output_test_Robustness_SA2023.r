# output.R - DESC
# /output.R

# Copyright Iago MOSQUEIRA (WMR), 2022
# Author: Iago MOSQUEIRA (WMR) <iago.mosqueira@wur.nl>
#
# Distributed under the terms of the EUPL-1.2


library(mse)

load("model/partial.Rdata")


# --- RESAMPLE with weighting

# GENERATE weights

results[, weight:=pvalue]

# GENERATE resamples
   
set.seed(47)

its <- 500

samps <- sample.int(dim(results)[1], size=its, prob=results[,weight],
  replace=TRUE)

# RESAMPLE objects based on weights TODO: MAKE neater, function?

stock <- iter(stock, samps)
dimnames(stock) <- list(iter=seq(its))

sr <- iter(sr, samps)
dimnames(sr) <- list(iter=seq(its))

refpts <- iter(refpts, samps)
dimnames(refpts)$iter <- seq(its)

indices <- iter(indices, samps)
indices <- lapply(indices, function(x) {
  dimnames(x) <- list(iter=seq(its))
  return(x)
  }
)

# STORE original iters

results <- results[samps, ]
results[, orig:=iter]
results[, iter:=seq(its)]


# --- DEVIANCES

iy <- 2020
fy <- 2046

# rho: fishlife:sword fish rho(rec) = 0.68
rho <- 0.68

# PAST deviances

lastdevs <- residuals(sr)[, ac(2010:2015)]

alldevs <- residuals(sr)[, ac(1970:2015)]

# 1. Autocorrelated with individual sigmaR per run

devsrho <- Reduce(combine, lapply(seq(its), function(x)
  ar1rlnorm(rho=rho, years=seq(2016, fy), iters=1,
  sdlog=results[x, Recr_sigma]) %*% lastdevs[, '2015',,,,x]))

dimnames(devsrho) <- list(age=1, iter=results$iter)

# 2. Autocorrelated moving to N(0,1)

devsmov <- window(lastdevs, end=fy)

var <- rlnorm(its, devsmov %=% 0, results[, Recr_sigma])

# rho <- FLQuant(rho,dim=c(1,1,1,1,1,500))

for(i in seq(2016, fy))
  devsmov[, ac(i)] <- rho * devsmov[, ac(i-1)] + var[, ac(i)] * sqrt(1 - rho^2)
  
plot(devsmov,worm=1:5) + geom_vline(xintercept = 2016)  

# 3. GENERATE lnorm(0, sigma_i) deviances

devs0 <- Reduce(combine, lapply(results[, Recr_sigma], function(x)
  rlnorm(1, FLQuant(0, dimnames=list(year=seq(2016, fy), age=1)), x)))

# SRR deviances

deviances <- FLQuants(
  N=append(lastdevs, devs0),
  RHO=append(lastdevs, devsrho),
  MOV=append(lastdevs, devsmov))

ggsave("./report/output/deviances.png",
plot(deviances,worm=1:3) + geom_vline(xintercept=c(2015, 2018), linetype=3) +
  geom_hline(yintercept=1, linetype=2, size=0.3)
)

# DEFAULT

residuals(sr) <- deviances$N


# --- UPDATE to 2024

range(stock, c("minfbar", "maxfbar")) <- c(2, 8)




################################################################################
### get the Rdev and Sel from the 2023 SA and replace the OM values for 2019:2022

load("./data/2023_sa_swo.rda")
dt <- read.csv("./data/2023SA/rec.csv")
names(dt)[1] <- "year"
# assign a SA run to each iteration
its <- sample(46,500,replace=T)

# selectivity in the 4 year projection period and in the simulation period
stkfwd <-fwdWindow(stock, end=2021)
harvest(stkfwd)[,ac(2017:2021)]  <- harvest(newsaswo)[,ac(2017:2021),,,,its]          # replace the last 5 years, since it is the bais for fwdWindow
stkfwd <-fwdWindow(stkfwd, end=fy)
discards.n(stkfwd)<-0
landings.n(stkfwd)[,ac(2022:fy)]  <-1

# rec deviates in the 4 year projectino period
dt <- read.csv("./data/2023SA/rec.csv")
names(dt)[1] <- "year"
dt<-dt[,-1]


test<-           FLQuant(as.vector(t(t(dt))),dim=c(1,dim(dt)[1],1,1,1,dim(dt)[2]))   
test<-expand(test,unit=c(1,2))        
test[,,1,,,]  <- test[,,2,,,]
dimnames(test)<-list(age = "0", year = ac(1970:2020), unit = c("F","M"),season="1", area="unique", iter= ac(1:47))
test<-exp(test)
           
srdev<-residuals(sr)
srdev[,ac(2010:2020)]  <- test[,ac(2010:2020),,,,its]
residuals(sr)<-srdev

# FWD 2019-20 based on total catches

# iotc.org, 23 jan 2023 : 2019= 35511.10 ; 2020 =	28854.51 and	2021 = 24070.88
# iotc.org, 4 march 2024 : 2019 = 35256.07357	; 2020 = 28783.15827 ;	2021 = 24528.1529; 	2022 = 23597.35909


stock <- fwd(stkfwd, sr=sr,
  control=fwdControl(year=c(2019, 2020 , 2021,2022), 
  quant="catch", value=c(35256.07357,	28783.15827,	24528.1529,	23597.35909)), deviances=residuals(sr))
 
#  F_2023 = F_20212
stock <- fwd(stock, sr=sr, control=fwdControl(year=2023, relYear=2022,
  quant="fbar", value=1, minAge=2, maxAge=8), deviances=residuals(sr))

# OM

om <- FLom(stock=stock, sr=sr, refpts=refpts,
  projection=mseCtrl(method=fwd.om))

# DEBUG
# vcov(sr(om)) <- hessian(sr(om))

# DEBUG: catch.wt in indices
catch.wt(indices[[1]]) <- stock.wt(stock)[, ac(1994:2018)]
catch.wt(indices[[2]]) <- stock.wt(stock)[, ac(1994:2018)]

# OEM observations

oem.obs <- list(stk=nounit(stock), idx=lapply(indices, fwdWindow, end=fy))

# here read in the csv file CPUE2023 and replace indices 
# with the new values (before computing the q values

indices2023 <- read.csv("./data/CPUEs_2023.csv")
#check 
plot(index(oem.obs$idx[["UJPLL_NW"]])[,ac(1994:2022),,,,1])  +  geom_line(data= subset(indices2023 ,  fleet2 == "UJPLL_NW"),aes(Yr,cpue),colour="red")   + ggtitle("UJPLL_NW")
plot(index(oem.obs$idx[["UTWLL_NW"]])[,ac(1994:2022),,,,1])  +  geom_line(data= subset(indices2023 ,  fleet2 == "UTWLL_NW"),aes(Yr,cpue),colour="red")   + ggtitle("UTWLL_NW")


index(oem.obs$idx[["UJPLL_NW"]])[,ac(1994:2022)]  <-    subset(indices2023 ,  fleet2 == "UJPLL_NW")$cpue
index(oem.obs$idx[["UTWLL_NW"]])[,ac(2005:2022)]  <-    subset(indices2023 ,  fleet2 == "UTWLL_NW")$cpue



# q deviances With llq increase

q.devs <- FLQuants(lapply(setNames(nm=names(indices)), function(i)
  Reduce(combine, lapply(results$llq, function(x)
    FLQuant(rlnorm(24, log(x ^ seq(24)), 0.2),
    dimnames=list(age='all', year=2023:2046))))))

# No q increase

idx.devs <- lapply(oem.obs$idx, function(x)
  rlnorm(500, index.q(x) %=% 0, 0.2))

idx.devs[[1]][,ac(1994:2022)] <- 1
idx.devs[[2]][,ac(1994:2022)] <- 1

# UPDATE indices

#oem.obs$idx[[1]][, ac(2019:2022)] <- 
#  survey(nounit(stock[, ac(2019:2022)]), oem.obs$idx[[1]][, ac(2019:2022)])
#index(oem.obs$idx[[1]][, ac(2019:2022)]) <-
#  index(oem.obs$idx[[1]][, ac(2019:2022)]) * q.devs[[1]][, ac(2019:2022)]
#
#oem.obs$idx[[2]][, ac(2019:2022)] <- 
#  survey(nounit(stock[, ac(2019:2022)]), oem.obs$idx[[2]][, ac(2019:2022)])
#index(oem.obs$idx[[2]][, ac(2019:2022)]) <-
#  index(oem.obs$idx[[2]][, ac(2019:2022)]) * q.devs[[2]][, ac(2019:2022)]
#
# OEM deviances, CHECK SDs

stock.devs <- FLQuants(
  catch.n=rlnorm(500, catch.n(oem.obs$stk) %=% 0, 0.2)
)

stock.devs[[1]][,ac(1950:2022)] <- 1


# CONSTRUCT OEM

oem <- FLoem(observations=oem.obs, method=sampling.oem,
  deviances=list(stk=stock.devs, idx=q.devs))

# SAVE

save(om, oem, results, deviances, file="output/om_Robstness_SA2023.Rdata", compress="xz")


# plot difference with base case
om_rob<- om

load("output/om.Rdata")


## MAKE a plot that combines the OM and the assessmentq()
x<-window(om ,end =2023)
y<-window(om_rob ,end =2023)

x<-om
y<-assess



 dat <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(x))  / refpts(x)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(x)) / refpts(x)$FMSY)
  
 dat2 <- FLQuants(
  `SB/SB[MSY]`=unitSums(ssb(y))  / refpts(y)$SBMSY,
  `F/F[MSY]`=  unitMeans(fbar(y)) / refpts(y)$FMSY) 
  
dat<-as.data.frame(dat,drop=T) 
dat2<-as.data.frame(dat2,drop=T)   

dat$basis <- "OM"
dat2$basis <- "alt_OM"

dat <- rbind(dat,dat2)
#dat <- subset(dat, year < 2024)

d1<-aggregate(data~ year + qname + basis , dat,median)
names(d1)[4] <- "median"
d2<-aggregate(data~ year + qname + basis , dat,function(x) quantile(x,0.05))
names(d2)[4] <- "Q05"
d3<-aggregate(data~ year + qname + basis , dat,function(x) quantile(x,0.90))
names(d3)[4] <- "Q95"

dd<-merge(d1,merge(d2,d3,all=T),all=T)


  p1 <- ggplot(subset(dd,qname=="SB/SB[MSY]"),aes(year,median,col=basis)) + geom_ribbon(aes(ymin = Q05,ymax=Q95,fill=basis),alpha=0.3) + 
    geom_line() + geom_hline(yintercept=1, linetype=2) + ylim(c(0,11))  + ylab("SB/SB[MSY]")


  p2 <- ggplot(subset(dd,qname=="F/F[MSY]"),aes(year,median,col=basis)) + geom_ribbon(aes(ymin = Q05,ymax=Q95,fill=basis),alpha=0.3) + 
    geom_line() + geom_hline(yintercept=1, linetype=2)  + ylim(c(0,3))  + ylab("F/F[MSY]")


  fin <- data.table(dat)[year == 2023, ]
  fin[,y_min := 0]
  fin[,y_max:= 6, by = qname]


  p3 <- ggplot(subset(fin,qname=="SB/SB[MSY]"), aes(y=data,fill = basis)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 6))+
    theme(legend.position="none") +
    xlim(c(0,2))   +    ylim(c(0,11)) +
    xlab("") + ylab("SB(2021)/SB[MSY]")+
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)
      
      
     
  p4 <- ggplot(subset(fin,qname=="F/F[MSY]"), aes(y=data,fill = basis)) +
    geom_histogram(aes(x=after_stat(density)), fill="gray",
      color="gray", alpha=0.2, bins=30) +
    geom_density(alpha=0.2) +
    facet_grid(qname~., scales="free_y") +
    geom_blank(aes(y = 0)) +
    geom_blank(aes(y = 25))+
    theme(legend.position="none") +
    xlim(c(0,25))   +    ylim(c(0,3)) +
    xlab("") +  ylab("F(2021)/F[MSY]")+
    theme(
      strip.text.y = element_blank(),
      axis.text.y=element_blank(),
      axis.ticks.x=element_blank(),
      plot.margin=unit(c(5.5,5.5,5.5,0), 'pt'))   +
      geom_hline(yintercept =1)
      

  # ASSEMBLE plot with c(4,1) horizontal proportions
 library(patchwork)
ggsave("report/OMcomparisonaltOM.png",       
  p1 + p3 + p2 + p4 + plot_layout(widths = c(4, 4))     )


